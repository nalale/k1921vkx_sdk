/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Запускаются часы реального времени, затем текущая дата и время
  *          выводятся через printf.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    04.12.2015
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2015 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"
#include "retarget_conf.h"

void SystemInit()
{}

void PeriphInit()
{
    // инициализация printf
    retarget_init();
}



int main()
{
    PeriphInit();


    RTC_Time_TypeDef RTC_TimeStruct;
    RTC_Date_TypeDef RTC_DateStruct;

    // инициализируем время и дату
    RTC_TimeStruct.RTC_Psecond = 0;
    RTC_TimeStruct.RTC_Second = 0;
    RTC_TimeStruct.RTC_Minute = 1;
    RTC_TimeStruct.RTC_Hour = 12;
    RTC_DateStruct.RTC_Weekday = RTC_Weekday_Friday;
    RTC_DateStruct.RTC_Day = 0x4;
    RTC_DateStruct.RTC_Month = RTC_Month_December;
    RTC_DateStruct.RTC_Year = 0x15;
    RTC_SetTime(RTC_Format_BCD, &RTC_TimeStruct);
    RTC_SetDate(RTC_Format_BCD, &RTC_DateStruct);

    // считываем текущие время и дату
    RTC_GetTime(RTC_Format_BIN, &RTC_TimeStruct);
    RTC_GetDate(RTC_Format_BIN, &RTC_DateStruct);
    printf("Current date: %d.%d.%d\n", RTC_DateStruct.RTC_Day, RTC_DateStruct.RTC_Month, 2000+RTC_DateStruct.RTC_Year);
    printf("Current time: %d:%d:%d\n", RTC_TimeStruct.RTC_Hour, RTC_TimeStruct.RTC_Minute, RTC_TimeStruct.RTC_Second);

    while(1);
}

