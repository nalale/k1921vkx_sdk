/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Пример, использующий функции драйвера GPIO для того чтобы
  *          переключать порты.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    26.07.2015
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2016 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"

/* Светодиоды разбросаны по разным портам */
#define LED0_PORT       NT_GPIOE
#define LED0_PIN_MASK   (1<<8)
#define LED1_PORT       NT_GPIOE
#define LED1_PIN_MASK   (1<<9)
#define LED2_PORT       NT_GPIOF
#define LED2_PIN_MASK   (1<<2)
#define LED3_PORT       NT_GPIOG
#define LED3_PIN_MASK   (1<<8)
#define LED4_PORT       NT_GPIOH
#define LED4_PIN_MASK   (1<<3)
#define LED5_PORT       NT_GPIOH
#define LED5_PIN_MASK   (1<<4)
#define LED6_PORT       NT_GPIOH
#define LED6_PIN_MASK   (1<<5)
#define LED7_PORT       NT_GPIOH
#define LED7_PIN_MASK   (1<<7)

void delay(uint32_t val)
{
    for(uint32_t i = 0; i < val; i++)
    {
        __NOP();
    }
}

void SystemInit()
{}

void PeriphInit()
{
    GPIO_Init_TypeDef GPIOInit;
    GPIO_StructInit(&GPIOInit);
    GPIOInit.GPIO_Dir = GPIO_Dir_Out;
    GPIOInit.GPIO_Out = GPIO_Out_En;

    /* GPIOE */
    GPIOInit.GPIO_Pin = LED0_PIN_MASK | LED1_PIN_MASK;
    GPIO_Init(NT_GPIOE, &GPIOInit);

    /* GPIOF */
    GPIOInit.GPIO_Pin = LED2_PIN_MASK;
    GPIO_Init(NT_GPIOF, &GPIOInit);

    /* GPIOG */
    GPIOInit.GPIO_Pin = LED3_PIN_MASK;
   GPIO_Init(NT_GPIOG, &GPIOInit);

    /* GPIOH */
    GPIOInit.GPIO_Pin = LED4_PIN_MASK | LED5_PIN_MASK | LED6_PIN_MASK | LED7_PIN_MASK;
    GPIO_Init(NT_GPIOH, &GPIOInit);
}

int main()
{
    PeriphInit();


    uint32_t delay_value = 500000;
    while(1)
    {
        GPIO_ToggleBits(NT_GPIOE, LED0_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOE, LED1_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOF, LED2_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOG, LED3_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOH, LED4_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOH, LED5_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOH, LED6_PIN_MASK);
        delay(delay_value);
        GPIO_ToggleBits(NT_GPIOH, LED7_PIN_MASK);
        delay(delay_value);
    };
}
