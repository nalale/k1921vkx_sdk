/**
  ******************************************************************************
  * @file    main.c
  *
  * @brief   Пример, использующий функции драйвера CAP, чтобы организовать
  *          2 ШИМ сигнала, генерирующиеся в противофазе.
  *
  *          Компилятор: GCC ARM 4.9.3
  *          Среда: Qt Creator 3.4.2
  *
  * @author  НИИЭТ
  *             - Богдан Колбов (bkolbov), kolbov@niiet.ru
  * @date    18.03.2016
  *
  ******************************************************************************
  * @attention
  *
  * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
  * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
  * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
  * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
  * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
  * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
  * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
  * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
  * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
  * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
  *
  * <h2><center>&copy; 2016 ОАО "НИИЭТ"</center></h2>
  ******************************************************************************
  */

#include "niietcm4.h"
#include "system_K1921VK01T.h"

void SystemInit()
{}

void PeriphInit()
{
    // инициализация блоков захвата
    RCC_PeriphRstCmd(RCC_PeriphRst_CAP0, ENABLE);
    RCC_PeriphRstCmd(RCC_PeriphRst_CAP1, ENABLE);

    // выбираем PWM режим
    CAP_Init_TypeDef CAP_InitStruct;
    CAP_StructInit(&CAP_InitStruct);
    CAP_InitStruct.CAP_Mode = CAP_Mode_PWM;
    CAP_InitStruct.CAP_SyncCmd = ENABLE;
    CAP_Init(NT_CAP0, &CAP_InitStruct);
    CAP_Init(NT_CAP1, &CAP_InitStruct);

    // настраиваем PWM режим
    // 2 меандра в противофазе
    CAP_PWM_Init_TypeDef CAP_PWM_InitStruct;
    CAP_PWM_StructInit(&CAP_PWM_InitStruct);
    CAP_PWM_InitStruct.CAP_PWM_Period = 0x3000;
    CAP_PWM_InitStruct.CAP_PWM_Compare = 0x1800;
    CAP_PWM_InitStruct.CAP_PWM_Polarity = CAP_PWM_Polarity_Pos;
    CAP_PWM_Init(NT_CAP0, &CAP_PWM_InitStruct);
    CAP_PWM_InitStruct.CAP_PWM_Polarity = CAP_PWM_Polarity_Neg;
    CAP_PWM_Init(NT_CAP1, &CAP_PWM_InitStruct);

    // включаем счетчики - ШИМ начинает генерировать
    CAP_TimerCmd(NT_CAP0, ENABLE);
    CAP_TimerCmd(NT_CAP1, ENABLE);

    // синхронизируем счетчики для точной противофазы
    // в оба счетчика запишутся нули
    CAP_SwSync(NT_CAP0);

    // инициализация выводов блоков CAP0 и CAP1
    // ШИМ сигналы появятся на выводах микроконтроллера E[4] и E[5]
    GPIO_Init_TypeDef GPIO_InitStruct;
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.GPIO_Dir = GPIO_Dir_Out;
    GPIO_InitStruct.GPIO_Out = GPIO_Out_En;
    GPIO_InitStruct.GPIO_AltFunc = GPIO_AltFunc_2;
    GPIO_InitStruct.GPIO_Mode = GPIO_Mode_AltFunc;
    GPIO_InitStruct.GPIO_Pin = GPIO_Pin_4 | GPIO_Pin_5;
    GPIO_Init(NT_GPIOE, &GPIO_InitStruct);
}

int main()
{
    PeriphInit();


    while(1);
}
