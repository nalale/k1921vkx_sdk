/*==============================================================================
 * Генерация синуса 50 Гц с помощью ШИМ 20 кГц и загрузки через DMA.
 * Требуется внешний фильтр (например, соответсвующая RC-цепь) для выделения
 * синуса.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK035.h"
#include "bsp.h"
#include "retarget_conf.h"
#include <math.h>

//-- Defines -------------------------------------------------------------------
#define PWM_CLK_FREQ 25000000                // Гц
#define PWM_FREQ 20000                       // Гц
#define PWM_PERIOD (PWM_CLK_FREQ / PWM_FREQ) // такты
#define SIN_FREQ 50                          // Гц
#define SIN_N (PWM_FREQ / SIN_FREQ)
#define SIN_A (PWM_CLK_FREQ / (PWM_FREQ * 2))
#define M_PI 3.14159265358979323846

//-- Types ---------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------
static volatile uint16_t sin_arr[SIN_N];
static volatile DMA_CtrlData_TypeDef DMA_CTRLDATA __attribute__((aligned(1024)));

//-- Peripheral init functions -------------------------------------------------
void pwm_init()
{
    //Инициализация таблицы синуса
    for (uint32_t i = 0; i < SIN_N; i++)
        sin_arr[i] = (uint16_t)(SIN_A * (sinf((i + 2) * (float)M_PI / (SIN_N / 2)) + 1.0f));

    // Настройка портов PWM
    RCU->HCLKCFG_bit.GPIOAEN = 1;
    RCU->HRSTCFG_bit.GPIOAEN = 1;
    GPIOA->DENSET |= GPIO_DENSET_PIN8_Msk;
    GPIOA->ALTFUNCSET |= GPIO_ALTFUNCSET_PIN8_Msk;
    // Настройка счёта PWM
    RCU->PCLKCFG_bit.PWM0EN = 1;
    RCU->PRSTCFG_bit.PWM0EN = 1;
    PWM0->TBCTL_bit.CLKDIV = PWM_TBCTL_CLKDIV_Div1;       //100 МГц
    PWM0->TBCTL_bit.HSPCLKDIV = PWM_TBCTL_HSPCLKDIV_Div4; //25 МГц
    PWM0->CMPA_bit.CMPA = SIN_A - 1;
    PWM0->CMPCTL_bit.LOADAMODE = PWM_CMPCTL_LOADBMODE_CTREqZero;
    PWM0->TBPRD = PWM_PERIOD - 1;
    PWM0->AQCTLA_bit.ZRO = PWM_AQCTLA_ZRO_Set;
    PWM0->AQCTLA_bit.CAU = PWM_AQCTLA_CAU_Clear;
    // Интерфейс с DMA
    PWM0->ETSEL_bit.DRQASEL = PWM_ETSEL_DRQASEL_CTREqZero;
    PWM0->ETSEL_bit.DRQAEN = 1;

    //Настройка DMA
    SIU->DMAMUX_bit.SRCSEL13 = SIU_DMAMUX_SRCSEL13_PWM0A;
    DMA->BASEPTR = (uint32_t)(&DMA_CTRLDATA);
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].SRC_DATA_END_PTR = (uint32_t)(&(sin_arr[SIN_N - 1]));
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.SRC_SIZE = 1; // 16 бит
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.SRC_INC = 1;  // 16 бит
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].DST_DATA_END_PTR = (((uint32_t) & (PWM0->CMPA)) + 2);
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.DST_SIZE = 1; // 16 бит
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.DST_INC = 3;  // без инкремента
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.R_POWER = 0;
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.N_MINUS_1 = SIN_N - 1;
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.CYCLE_CTRL = 1; // базовый
    DMA->ENSET = DMA_ENSET_CH13_Msk;
    DMA->CFG_bit.MASTEREN = 1;
    NVIC_EnableIRQ(DMA_CH13_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    pwm_init();
    SysTick_Config(10000000);
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    printf("Start sine generation ...\n");

    // Включаем счетчик PWM0
    SIU->PWMSYNC_bit.PRESCRST = (1 << 0);

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle();
}

void DMA_CH13_IRQHandler()
{
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.N_MINUS_1 = SIN_N - 1;
    DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_PWM0A].CHANNEL_CFG_bit.CYCLE_CTRL = 1;
    DMA->ENSET = DMA_ENSET_CH13_Msk;
}
