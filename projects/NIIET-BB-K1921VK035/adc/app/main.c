/*==============================================================================
 * Постоянный опрос всех каналов АЦП с усреднением по каждому из них.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK035.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void adc_init()
{
    //Инициализация ADC
    RCU->ADCCFG_bit.CLKSEL = RCU_ADCCFG_CLKSEL_PLLCLK;
    RCU->ADCCFG_bit.DIVN = 0x1; // div4
    RCU->ADCCFG_bit.DIVEN = 0x1;
    RCU->ADCCFG_bit.CLKEN = 0x1;
    RCU->ADCCFG_bit.RSTDIS = 0x1;

    //Настройка модуля АЦП
    ADC->ACTL_bit.ADCEN = 0x1;
    //Настройка секвенсора 0
    // CH0, CH1, CH2, CH3.
    ADC->EMUX_bit.EM0 = ADC_EMUX_EM0_SwReq;
    ADC->SEQSYNC = ADC_SEQSYNC_SYNC0_Msk;
    ADC->SEQ[0].SCCTL_bit.ICNT = 3;
    ADC->SEQ[0].SRQCTL_bit.RQMAX = 0x3;
    ADC->SEQ[0].SRQCTL_bit.QAVGVAL = ADC_SEQ_SRQCTL_QAVGVAL_Average64;
    ADC->SEQ[0].SRQCTL_bit.QAVGEN = 1;
    ADC->SEQ[0].SRQSEL_bit.RQ0 = 0x0;
    ADC->SEQ[0].SRQSEL_bit.RQ1 = 0x1;
    ADC->SEQ[0].SRQSEL_bit.RQ2 = 0x2;
    ADC->SEQ[0].SRQSEL_bit.RQ3 = 0x3;
    ADC->SEQEN_bit.SEQEN0 = 1;
    while (!ADC->ACTL_bit.ADCRDY) {
    };

    // Прерывание
    ADC->IM_bit.SEQIM0 = 1;
    NVIC_EnableIRQ(ADC_SEQ0_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    BSP_Btn_Init();
    adc_init();
    SysTick_Config(10000000);
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
static volatile float avdd = 3.3f;
int main()
{
    periph_init();
    printf("AVDD = %1.1fV\n", avdd);
    printf("Press button to perform ADC 0-3 channels conversion\n");

    while (1) {
        if (BSP_Btn_IsPressed()) {
            ADC->SEQSYNC |= ADC_SEQSYNC_GSYNC_Msk;
        }
    }
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle();
}

void ADC_SEQ0_IRQHandler()
{
    uint32_t ch_res[] = { 0x777, 0x777, 0x777, 0x777 };
    uint32_t i = 0;

    ADC->IC_bit.SEQIC0 = 1;

    while (ADC->SEQ[0].SFLOAD) {
        ch_res[i++] = ADC->SEQ[0].SFIFO;
    }

    printf("0x%03x, 0x%03x, 0x%03x, 0x%03x | ", ch_res[0], ch_res[1], ch_res[2], ch_res[3]);
    printf("%04d, %04d, %04d, %04d | ", ch_res[0], ch_res[1], ch_res[2], ch_res[3]);
    printf("%.03f, %.03f, %.03f, %.03f    \n",
           (ch_res[0] * avdd) / 4096,
           (ch_res[1] * avdd) / 4096,
           (ch_res[2] * avdd) / 4096,
           (ch_res[3] * avdd) / 4096);
}
