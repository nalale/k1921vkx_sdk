/*==============================================================================
 * Пример передачи стандартных CAN-сообщений (3 передачи)
 * CAN0:
 *   - объекты для передачи (FIFO): 0 - базовый, 1-3 - вспомогательные
 * CAN1:
 *   - объекты для приёма: 58-60
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2020 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK035.h"
#include "bsp.h"
#include "retarget_conf.h"
#include <stdio.h>

//-- Defines -------------------------------------------------------------------

//-- Types ---------------------------------------------------------------------
typedef enum {
    CAN_OPERATION_TX,
    CAN_OPERATION_RX,
    CAN_OPERATION_TXRX
} CAN_Operation_TypeDef;

typedef enum {
    CAN_MESSAGE_REMOTE,
    CAN_MESSAGE_COMMON
} CAN_Message_TypeDef;

//-- Variables -----------------------------------------------------------------
static uint32_t OK_MODATAL = 0;
static uint32_t ERR_MODATAL = 0;
static uint32_t OK_MODATAH = 0;
static uint32_t ERR_MODATAH = 0;
volatile static uint32_t IRQ_COUNT = 0;

//-- CAN service functions -----------------------------------------------------
void CAN_Object_Location(uint32_t obj_first_num, uint32_t obj_last_num,
                         uint32_t list_num)
{
    unsigned int x;

    // LOCATION OBJECTS TO THE LISTS
    for (x = obj_first_num; x <= obj_last_num; x++) {
        // PANCMD_field=0x02-static location objects to one of the CAN-lists
        CAN->PANCTR = (0x2 << CAN_PANCTR_PANCMD_Pos) |
                      (x << CAN_PANCTR_PANAR1_Pos) |
                      (list_num << CAN_PANCTR_PANAR2_Pos);

        while ((CAN->PANCTR_bit.BUSY) | (CAN->PANCTR_bit.RBUSY)) {
        };
    }
}

void CAN_Object_Config(uint32_t obj_num, CAN_Operation_TypeDef op_type,
                       CAN_Message_TypeDef msg_type)
{
    if (op_type == CAN_OPERATION_TX) {
        if (msg_type == CAN_MESSAGE_COMMON)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_SETDIR_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN0_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN1_Msk;
        else if (msg_type == CAN_MESSAGE_REMOTE)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_RESDIR_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN0_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN1_Msk;
    } else if (op_type == CAN_OPERATION_RX) {
        if (msg_type == CAN_MESSAGE_COMMON)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_RESDIR_Msk | CANMSG_Msg_MOCTR_SETRXEN_Msk;
        else if (msg_type == CAN_MESSAGE_REMOTE)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_SETDIR_Msk | CANMSG_Msg_MOCTR_SETRXEN_Msk;
    } else if (op_type == CAN_OPERATION_TXRX) {
        if (msg_type == CAN_MESSAGE_COMMON)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_SETDIR_Msk | CANMSG_Msg_MOCTR_SETTXEN0_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN1_Msk | CANMSG_Msg_MOCTR_SETRXEN_Msk;
        else if (msg_type == CAN_MESSAGE_REMOTE)
            CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_RESDIR_Msk | CANMSG_Msg_MOCTR_SETTXEN0_Msk |
                                         CANMSG_Msg_MOCTR_SETTXEN1_Msk | CANMSG_Msg_MOCTR_SETRXEN_Msk;
    }
}

void CAN_Object_Transmit(uint32_t obj_num)
{
    CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_SETTXRQ_Msk | CANMSG_Msg_MOCTR_SETMSGVAL_Msk;
}

void CAN_Object_Receive(uint32_t obj_num)
{
    CANMSG->Msg[obj_num].MOCTR = CANMSG_Msg_MOCTR_SETMSGVAL_Msk;
}

void CAN_CompareData(uint32_t tx_obj_num, uint32_t rx_obj_num)
{
    if (CANMSG->Msg[rx_obj_num].MODATAL != CANMSG->Msg[tx_obj_num].MODATAL)
        ERR_MODATAL += 1;
    else
        OK_MODATAL += 1;

    if (CANMSG->Msg[rx_obj_num].MODATAH != CANMSG->Msg[tx_obj_num].MODATAH)
        ERR_MODATAH += 1;
    else
        OK_MODATAH += 1;
}

void CAN_CompareDataBlock(uint32_t first_tx_obj_num, uint32_t first_rx_obj_num,
                          uint32_t trans_total)
{
    for (uint32_t x = 0; x < trans_total; x++) {
        CAN_CompareData(first_tx_obj_num, first_rx_obj_num);
        first_tx_obj_num += 1;
        first_rx_obj_num += 1;
    }
}

//-- Peripheral init functions -------------------------------------------------
void can_init()
{
    // IO
    RCU->HCLKCFG_bit.GPIOBEN = 1;
    RCU->HRSTCFG_bit.GPIOBEN = 1;
    GPIOB->ALTFUNCSET = 0xF000;
    GPIOB->DENSET = 0xF000;
    // Clock and reset
    RCU->HCLKCFG_bit.CANEN = 1;
    RCU->HRSTCFG_bit.CANEN = 1;
    CAN->CLC_bit.DISR = 0;

    while ((CAN->CLC_bit.DISS) & (CAN->PANCTR_bit.PANCMD)) {
    };
    CAN->FDR = (0x1 << CAN_FDR_DM_Pos) |
               (0x3FF << CAN_FDR_STEP_Pos); // normal divider mode

    // Enable the change configuration of the CAN node's
    // CAN0 and CAN1 are disconnected from the bus
    CAN->Node[0].NCR = CAN_Node_NCR_CCE_Msk | CAN_Node_NCR_INIT_Msk;
    CAN->Node[0].NBTR = (0x4 << CAN_Node_NBTR_TSEG2_Pos) | (0x5 << CAN_Node_NBTR_TSEG1_Pos) |
                        (0x2 << CAN_Node_NBTR_SJW_Pos) | (0xE << CAN_Node_NBTR_BRP_Pos);
    CAN->Node[1].NCR = CAN_Node_NCR_CCE_Msk | CAN_Node_NCR_INIT_Msk;
    CAN->Node[1].NBTR = (0x4 << CAN_Node_NBTR_TSEG2_Pos) | (0x5 << CAN_Node_NBTR_TSEG1_Pos) |
                        (0x2 << CAN_Node_NBTR_SJW_Pos) | (0xE << CAN_Node_NBTR_BRP_Pos);

    // CAN0, CAN1 is connected with the bus, node's interrupts are enable
    CAN->Node[0].NCR = CAN_Node_NCR_TRIE_Msk;
    CAN->Node[1].NCR = CAN_Node_NCR_TRIE_Msk;

    // choosing number lines for node's interrupts
    CAN->Node[0].NIPR = (0xC << CAN_Node_NIPR_TRINP_Pos);
    CAN->Node[1].NIPR = (0xF << CAN_Node_NIPR_TRINP_Pos);

    // NVIC interrupts
    NVIC_EnableIRQ(CAN1_IRQn);
    NVIC_EnableIRQ(CAN2_IRQn);
    NVIC_EnableIRQ(CAN4_IRQn);
    NVIC_EnableIRQ(CAN12_IRQn);
    NVIC_EnableIRQ(CAN15_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    can_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    uint32_t L;
    uint32_t H;
    uint32_t temp_MOAR;

    periph_init();

    printf("Set object location to the lists\n");
    while (CAN->PANCTR_bit.BUSY) {
    };
    // Location 0-3 objects to the 1 list (0 node)
    CAN_Object_Location(0, 3, 1);
    // Location 58-60 objects to the 2 list (1 node)
    CAN_Object_Location(58, 60, 2);

    printf("Prepare objects for transmit\n");
    //Build CAN0 FIFO with 0 - FIFO basic object, 1-3 - FIFO slave objects
    CANMSG->Msg[0].MOFCR_bit.MMC = CANMSG_Msg_MOFCR_MMC_TXObj;
    CANMSG->Msg[0].MOFGPR_bit.CUR = 1;
    CANMSG->Msg[0].MOFGPR_bit.BOT = 1;
    CANMSG->Msg[0].MOFGPR_bit.CUR = 1;
    CANMSG->Msg[0].MOFGPR_bit.SEL = 1;
    CANMSG->Msg[0].MOFGPR_bit.TOP = 3;
    CANMSG->Msg[0].MOCTR = CANMSG_Msg_MOCTR_SETMSGVAL_Msk;
    CANMSG->Msg[0].MOFCR_bit.OVIE = 1; // interrupt on FIFO overflow (MOFGPR.SEL==MOFGPR.CUR)
    CANMSG->Msg[0].MOIPR_bit.RXINP = 2; // line 2

    temp_MOAR = (0x2 << CANMSG_Msg_MOAR_PRI_Pos) | // filtration by identifier
                CANMSG_Msg_MOAR_IDE_Msk;           // extended identifier
    for (uint32_t x = 1; x <= 3; x++) {
        //CAN_Object_Config(x, CAN_OPERATION_TX, CAN_MESSAGE_COMMON);
        if (x == 1)
            CANMSG->Msg[x].MOCTR = CANMSG_Msg_MOCTR_SETDIR_Msk |
                                   CANMSG_Msg_MOCTR_SETTXEN0_Msk |
                                   CANMSG_Msg_MOCTR_SETTXEN1_Msk;
        else
            CANMSG->Msg[x].MOCTR = CANMSG_Msg_MOCTR_SETDIR_Msk |
                                   CANMSG_Msg_MOCTR_SETTXEN0_Msk;
        CANMSG->Msg[x].MOFGPR_bit.CUR = 0; // all slaves must point to base object
        // identifiers:  0xA0010000(0 obj)/0xA0020000(1 obj)/0xA0030000(2 obj)
        temp_MOAR = temp_MOAR + 0x00010000;
        CANMSG->Msg[x].MOAR = temp_MOAR;
        // Set lenth of data in the CAN-object, enable interrupts
        CANMSG->Msg[x].MOFCR =
            (0x8 << CANMSG_Msg_MOFCR_DLC_Pos) | CANMSG_Msg_MOFCR_TXIE_Msk |
            CANMSG_Msg_MOFCR_MMC_SlaveTXObj << CANMSG_Msg_MOFCR_MMC_Pos;
        // number line of CAN-object's successfull transmission interrupt
        CANMSG->Msg[x].MOIPR = (0x1 << CANMSG_Msg_MOIPR_TXINP_Pos);
    }

    printf("Prepare objects for receive\n");
    temp_MOAR = (0x2 << CANMSG_Msg_MOAR_PRI_Pos) | // filtration by identifier
                CANMSG_Msg_MOAR_IDE_Msk;           // extended identifier

    for (uint32_t x = 58; x <= 60; x++) {
        CAN_Object_Config(x, CAN_OPERATION_RX, CAN_MESSAGE_COMMON);
        // identifiers: 0xA0010000(58 obj)/0xA0020000(59 obj)/0xA0030000(60 obj)
        temp_MOAR = temp_MOAR + 0x00010000;
        CANMSG->Msg[x].MOAR = temp_MOAR;

        CANMSG->Msg[x].MOFCR = (0x8 << CANMSG_Msg_MOFCR_DLC_Pos) |
                               CANMSG_Msg_MOFCR_RXIE_Msk;
        CANMSG->Msg[x].MOIPR = (0x4 << CANMSG_Msg_MOIPR_RXINP_Pos);
    }

    printf("Write data for transmit\n");
    L = 0x1000F000;
    H = 0xF0101010;
    for (uint32_t x = 1; x <= 3; x++) {
        CANMSG->Msg[x].MODATAL = L;
        CANMSG->Msg[x].MODATAH = H;
        L = L + 0x00010001;
        H = H + 0x00010001;
    }

    printf("Start transmission!\n");
    for (uint32_t x = 58; x <= 60; x++)
        CAN_Object_Receive(x);

    for (uint32_t x = 1; x <= 3; x++)
        CAN_Object_Transmit(x);


    // waiting 3 tx + 3 rx + 3 irq * 2 nodes + 1 FIFO overflow = 13 irqs
    while (IRQ_COUNT < 13) {
    };

    printf("Check results ...\n");
    CAN_CompareDataBlock(1, 58, 3);

    if ((OK_MODATAL == 3) & (OK_MODATAH == 3)) {
        printf("All data transmitted successfuly!\n");
        BSP_LED_On();
    }

    while (1) {
    };
}

//-- IRQ handlers --------------------------------------------------------------
// objects
void CAN1_IRQHandler(void)
{
    IRQ_COUNT++;
}
void CAN2_IRQHandler(void)
{
    IRQ_COUNT++;
}
void CAN4_IRQHandler(void)
{
    IRQ_COUNT++;
}

// nodes
void CAN12_IRQHandler(void)
{
    IRQ_COUNT++;
}
void CAN15_IRQHandler(void)
{
    IRQ_COUNT++;
}
