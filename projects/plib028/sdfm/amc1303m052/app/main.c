/*==============================================================================
 * Получение данных с сигма-дельта модулятора AMC1303M052 каждую секунду
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void sdfm_init()
{
    SDFM_DF_Init_TypeDef SDFM_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;
    // SDFM_DATA2 - A8 af1
    // SDFM_CLK2  - A9 af1
    //настраиваем пины
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFuncNum = GPIO_AltFuncNum_1;
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_8 | GPIO_Pin_9;
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOA, GPIO_InitStruct.Pin, ENABLE);

    //тактирование и сброс SDFM
    RCU_AHBClkCmd(RCU_AHBClk_SDFM, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_SDFM, ENABLE);
    //режим демодуляции
    SDFM_DecoderConfig(SDFM_ChannelNum_2, SDFM_Decoder_SDR);
    //параметры фильтра данных
    SDFM_DF_StructInit(&SDFM_InitStruct);
    SDFM_InitStruct.Mode = SDFM_Mode_Sinc3;
    SDFM_InitStruct.OSR = 31; // range [-32768;32768]
    SDFM_InitStruct.FIFOEn = ENABLE;
    SDFM_DF_Init(SDFM_ChannelNum_2, &SDFM_InitStruct);
    //настройка прерывания
    SDFM_DF_ITFIFOLevelCmd(SDFM_ChannelNum_2, 6, ENABLE);
    SDFM_DF_ITConfig(SDFM_ChannelNum_2, SDFM_DF_ITSource_FIFOLevel);

    SDFM_Cmd(ENABLE);
    SDFM_ITCmd(ENABLE);
    NVIC_EnableIRQ(SDFM_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    sdfm_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
volatile uint32_t new_result = 0;
volatile int32_t result;
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); // 1ms tick

    while (1) {
        if (new_result) {
            new_result = 0;
            // IC Voltage range +-50 mV;
            // Current sensor 0.05 Ohm => Current range +-1000 mA
            // +-32768 is range for DOSR=31 and Sinc3 used
            printf("Current = %f mA (%0d)\n", (float)result * (1000.0f / 32768), (int16_t)result);
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter < 1000)
        tick_counter++;
    else {
        SDFM_DF_Cmd(SDFM_ChannelNum_2, ENABLE);
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    }
}

void SDFM_IRQHandler(void)
{
    if (SDFM_ITStatus(SDFM_ChannelNum_2, SDFM_ITStatus_DFFIFOLevel)) {
        SDFM_DF_GetData(SDFM_ChannelNum_2);
        SDFM_DF_GetData(SDFM_ChannelNum_2);
        result = (int32_t)SDFM_DF_GetData(SDFM_ChannelNum_2);
        result += (int32_t)SDFM_DF_GetData(SDFM_ChannelNum_2);
        result += (int32_t)SDFM_DF_GetData(SDFM_ChannelNum_2);
        result += (int32_t)SDFM_DF_GetData(SDFM_ChannelNum_2);
        result >>= 2;                   // 4 samples average
        SDFM_DF_Cmd(SDFM_ChannelNum_2, DISABLE);
        SDFM_ITStatusClear(SDFM_ChannelNum_2, SDFM_ITStatus_DFFIFOLevel);
        new_result = 1;
    }
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
