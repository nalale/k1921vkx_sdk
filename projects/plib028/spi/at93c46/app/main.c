/*==============================================================================
 * Пример использования SPI для работы с микросхемой EEPROM 93c46
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "at93c46.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_4, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_4, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_4);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    AT93C46_Init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick

    printf("AT93C46 Write Enable and Erase All\n");
    AT93C46_WriteEnable();
    AT93C46_EraseAll();

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write:\n");
    for (uint16_t i = 0; i < 8; i++) {
        uint16_t wdata = 0x8000 + i * 3 + (i * 4 << 8);
        AT93C46_Write(i, wdata);
        printf("    Addr[%d] = 0x%04x\n", (int)i, wdata);
    }

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Erase:\n");
    for (uint16_t i = 0; i < 8; i++) {
        if ((i + 1) % 2 != 0) {
            AT93C46_Erase(i);
            printf("    Addr[%d]\n", (int)i);
        }
    }

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write All 0xeeee\n");
    AT93C46_WriteAll(0xEEEE);

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Erase All and Write Disable\n");
    AT93C46_EraseAll();
    AT93C46_WriteDisable();

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write All 0xeeee\n");
    AT93C46_WriteAll(0xEEEE);

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_4);
    } else
        tick_counter++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
