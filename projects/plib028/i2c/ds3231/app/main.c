/*==============================================================================
 * Пример использования I2C для работы с микросхемой RTC DS3231.
 *
 * Применяется библиотека SMBUS, реализующая протоколы Master устройства.
 * Адресное пространство DS3231 имеет структуру-зеркало в RAM микроконтроллера.
 * Запись - значения заносятся в зеркало, затем вызываются функции SMBUS,
 * передающие данные в микросхему. Чтение - функции SMBUS читают данные  в
 * зеркало, из которого программа их извлекает.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "ds3231.h"
#include "plib028.h"
#include "retarget_conf.h"
#include "smbus.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------
static SMBus_SlaveDescriptor_TypeDef SMBUS_DS3231;
static DS3231_TypeDef DS3231_Regs __ALIGNED(DS3231_REGS_ALIGN_VAL);

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_4, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_4, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_4);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    SMBus_Init(SMBUS_MASTER1, 100000, 3400000);
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick
    // init I2C slave device
    SMBUS_DS3231.Address = DS3231_I2C_ADDR;
    SMBUS_DS3231.Mastercode = 0x00;
    SMBUS_DS3231.EnAddr10bit = 0;
    SMBUS_DS3231.EnPEC = 0;

    // init time
    DS3231_Regs.SEC = 0x00;
    DS3231_Regs.MIN = 0x43;
    DS3231_Regs.HOUR = 0x14;
    SMBus_BurstWrite(SMBUS_MASTER1, &SMBUS_DS3231,
                     DS3231_REG_PTR(DS3231_Regs.SEC), 3, DS3231_REG_ADDR(DS3231_Regs.SEC));
    while (SMBus_GetStatus(SMBUS_MASTER1, NULL) == SMBUS_STAT_BUSY) {
    };

    // read temp
    SMBus_BurstRead(SMBUS_MASTER1, &SMBUS_DS3231,
                    DS3231_REG_PTR(DS3231_Regs.TEMPH), 2, DS3231_REG_ADDR(DS3231_Regs.TEMPH));
    while (SMBus_GetStatus(SMBUS_MASTER1, NULL) == SMBUS_STAT_BUSY) {
    };
    printf("DS3231> Temperature is %d.%d\n", (int)DS3231_Regs.TEMPH, 25 * DS3231_Regs.TEMPL_bit.DATA);

    while (1) {
        //delay
        for (uint32_t i = 0; i < 15000000; i++)
            __NOP();
        // read time
        SMBus_BurstRead(SMBUS_MASTER1, &SMBUS_DS3231,
                        DS3231_REG_PTR(DS3231_Regs.SEC), 3, DS3231_REG_ADDR(DS3231_Regs.SEC));
        while (SMBus_GetStatus(SMBUS_MASTER1, NULL) == SMBUS_STAT_BUSY) {
        };
        printf("DS3231> Time is %02x:%02x:%02x\n", DS3231_Regs.HOUR, DS3231_Regs.MIN, DS3231_Regs.SEC);
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_7);
    } else
        tick_counter++;
}
// Master 1
void I2C1_IRQHandler(void)
{
    SMBus_Callback(SMBUS_MASTER1);
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
