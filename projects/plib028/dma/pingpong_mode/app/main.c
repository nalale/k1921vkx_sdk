/*==============================================================================
 * Тест режима пинг-понг DMA.
 * Пересылка 32-битных значений из массива в массив по цепочке
 * по 13 каналу. Выполняется в 4 этапа:
 *    - передача из массива 0 в массив 1 с использованием первичных данных
 *    - передача из массива 1 в массив 2 с использованием альтернативных данных
 *    - передача из массива 2 в массив 3 с использованием первичных данных
 *    - сверка идентичности данных в 0 и 3 массивах
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define __NOP10() \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP()

#define __NOP100() \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10()

#define TRANSFERS_TOTAL 16

//-- Variables -----------------------------------------------------------------
DMA_CtrlData_TypeDef DMA_CTRLDATA __ALIGNED(1024);
uint32_t array0[TRANSFERS_TOTAL];
uint32_t array1[TRANSFERS_TOTAL];
uint32_t array2[TRANSFERS_TOTAL];
uint32_t array3[TRANSFERS_TOTAL];
volatile uint32_t irq_status = 0;
DMA_ChannelInit_TypeDef DMA_ChannelInitStruct;

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}
void dma_init()
{
    /* Базовый указатель */
    DMA_BasePtrConfig((uint32_t)(&DMA_CTRLDATA));

    /* Инициализация канала */
    DMA_ChannelStructInit(&DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.SrcDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.DstDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.DstDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_8;
    DMA_ChannelInitStruct.TransfersTotal = TRANSFERS_TOTAL;
    DMA_ChannelInitStruct.Mode = DMA_Mode_PingPong;

    /* инициализация первичных данных для 1 этапа */
    DMA_ChannelInitStruct.SrcDataEndPtr = &array0[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[13], &DMA_ChannelInitStruct);
    /* инициализация альтернативных данных для 2 этапа */
    DMA_ChannelInitStruct.SrcDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&DMA_CTRLDATA.ALT_DATA.CH[13], &DMA_ChannelInitStruct);

    /* Инициализация контроллера */
    DMA_Init_TypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.Channel = DMA_Channel_13;
    DMA_InitStruct.ChannelEnable = ENABLE;
    DMA_Init(&DMA_InitStruct);
    DMA_MasterEnableCmd(ENABLE);

    /* NVIC init */
    NVIC_EnableIRQ(DMA_CH13_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    dma_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick

    /* инициализация массивов */
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        array0[i] = 0xDEADC0DE;
        array1[i] = 0xBEEFB00B;
        array2[i] = 0xCAFE6666;
        array3[i] = 0xBABEF00D;
    }

    /* старт 1 этапа */
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем первую половину данных */
    __NOP100();                       /* при переарбитрации нет прерывания, поэтому стоит немного подождать */
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем вторую половину данных */
    while (irq_status != DMA_Channel_13) {
    };
    irq_status = 0;
    /* инициализация первичных данных для 3 этапа */
    DMA_ChannelInitStruct.SrcDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array3[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[13], &DMA_ChannelInitStruct);

    /* старт 2 этапа */
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем первую половину данных */
    __NOP100();
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем вторую половину данных */
    while (irq_status != DMA_Channel_13) {
    };
    irq_status = 0;

    /* старт 3 этапа */
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем первую половину данных */
    __NOP100();
    DMA_SwRequestCmd(DMA_Channel_13); /* передаем вторую половину данных */
    while (irq_status != DMA_Channel_13) {
    };
    irq_status = 0;

    /* завершение цикла пинг-понг считыванием неверной структуры */
    DMA_SwRequestCmd(DMA_Channel_13);

    /* проверяем результаты */
    uint32_t check_error = 0;
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        printf("array0[%d]=0x%08x; array3[%d]=0x%08x\n",
               (int)i,
               (unsigned int)array0[i],
               (int)i,
               (unsigned int)array3[i]);
        if (array0[i] != array3[i])
            check_error++;
    }

    printf("All transfers done with %d errors!\n", (int)check_error);

    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}
void DMA_CH13_IRQHandler()
{
    irq_status |= DMA_Channel_13;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
