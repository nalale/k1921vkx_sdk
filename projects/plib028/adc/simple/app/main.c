/*==============================================================================
 * Запускается преобразование 0, 12, 24 и 36 каналов.
 * Каждые 4 измерения вызывается прерывание и результаты выводятся в printf().
 * Проводится три серии измерений таким образом.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7_0, ENABLE);
    GPIO_SetBits(GPIOA, GPIO_Pin_7_0);
}

void adc_init()
{
    ADC_SEQ_Init_TypeDef ADC_SEQ_InitStruct;

    PMU_ADCLDOCmd(PMU_ADCLDO_All, ENABLE);
    while (!(PMU_ADCLDOStatus(PMU_ADCLDOStatus_Ready0) &&
             PMU_ADCLDOStatus(PMU_ADCLDOStatus_Ready1) &&
             PMU_ADCLDOStatus(PMU_ADCLDOStatus_Ready2) &&
             PMU_ADCLDOStatus(PMU_ADCLDOStatus_Ready3))) {
    };

    RCU_ADCClkConfig(RCU_PeriphClk_PLLClk, 2, ENABLE); //200/6=33.3MHz
    RCU_ADCClkCmd(ENABLE);
    RCU_ADCRstCmd(ENABLE);

    //Включаем модули АЦП
    ADC_AM_ResolutionConfig(ADC_AM_Num_0, ADC_AM_Resolution_12bit);
    ADC_AM_CalibCmd(ADC_AM_Num_0, ENABLE);
    ADC_AM_Cmd(ADC_AM_Num_0, ENABLE);
    ADC_AM_ResolutionConfig(ADC_AM_Num_1, ADC_AM_Resolution_12bit);
    ADC_AM_CalibCmd(ADC_AM_Num_1, ENABLE);
    ADC_AM_Cmd(ADC_AM_Num_1, ENABLE);
    ADC_AM_ResolutionConfig(ADC_AM_Num_2, ADC_AM_Resolution_12bit);
    ADC_AM_CalibCmd(ADC_AM_Num_2, ENABLE);
    ADC_AM_Cmd(ADC_AM_Num_2, ENABLE);
    ADC_AM_ResolutionConfig(ADC_AM_Num_3, ADC_AM_Resolution_12bit);
    ADC_AM_CalibCmd(ADC_AM_Num_3, ENABLE);
    ADC_AM_Cmd(ADC_AM_Num_3, ENABLE);

    //Настройка секвенсора 0 (пока аналаговая часть инициализируется)
    ADC_SEQ_StructInit(&ADC_SEQ_InitStruct);
    ADC_SEQ_InitStruct.StartEvent = ADC_SEQ_StartEvent_SwReq;
    ADC_SEQ_InitStruct.SWStartEn = ENABLE;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_0] = ADC_CH_Num_0;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_1] = ADC_CH_Num_12;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_2] = ADC_CH_Num_24;
    ADC_SEQ_InitStruct.Req[ADC_SEQ_ReqNum_3] = ADC_CH_Num_36;
    ADC_SEQ_InitStruct.ReqMax = ADC_SEQ_ReqNum_3;
    ADC_SEQ_InitStruct.ReqAverage = ADC_SEQ_Average_16;
    ADC_SEQ_InitStruct.ReqAverageEn = ENABLE;
    ADC_SEQ_Init(ADC_SEQ_Num_0, &ADC_SEQ_InitStruct);
    ADC_SEQ_Cmd(ADC_SEQ_Num_0, ENABLE);

    ADC_SEQ_ITConfig(ADC_SEQ_Num_0, 3, ENABLE);
    ADC_SEQ_ITCmd(ADC_SEQ_Num_0, ENABLE);
    NVIC_EnableIRQ(ADC_SEQ0_IRQn);

    while (!(ADC_AM_ReadyStatus(ADC_AM_Num_0) &&
             ADC_AM_ReadyStatus(ADC_AM_Num_1) &&
             ADC_AM_ReadyStatus(ADC_AM_Num_2) &&
             ADC_AM_ReadyStatus(ADC_AM_Num_3))) {
    };
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    adc_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
volatile uint32_t adc_irq_count = 0;
int main()
{
    int res[4];

    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick
    printf("Start measures!\n");

    while (1) {
        if (adc_irq_count < 3) {
            ADC_SEQ_SwStartCmd();
            __WFI();
            printf("\nMeasure results:\n");
            printf("CH00  CH12  CH24  CH36\n");
            res[0] = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
            res[1] = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
            res[2] = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
            res[3] = ADC_SEQ_GetFIFOData(ADC_SEQ_Num_0);
            if (ADC_SEQ_GetFIFOLoad(ADC_SEQ_Num_0))
                printf ("Something gone wrong!\n");
            else
                printf("%04d  %04d  %04d  %04d\n", res[0], res[1], res[2], res[3]);
        }
    }
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter >= 200) {
        tick_counter = 0;
        GPIO_ToggleBits(GPIOA, GPIO_Pin_0);
    } else
        tick_counter++;
}

void ADC_SEQ0_IRQHandler()
{
    ADC_SEQ_ITStatusClear(ADC_SEQ_Num_0);
    adc_irq_count++;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
