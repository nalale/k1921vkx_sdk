/*==============================================================================
 * Вывод A[15] устанавливается в 0, затем в 1 и запускается сторожевой таймер.
 * При достижении таймером нуля в первый раз таймер будет
 * перезапущен и установится флаг прерывания таймера. А так как
 * обрабочик прерывания не разрешен - флаг не будет обработан, и после
 * второго достижения нуля микроконтроллер будет полностью
 * перезагружен (вывод A[7] при этом будет переведен в третье
 * состояние).
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib028.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define DELAY_VALUE 2000000

//-- Peripheral init functions -------------------------------------------------
void gpio_init()
{
    volatile uint32_t delay = DELAY_VALUE;

    while (delay) {
        delay--;
    }

    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_OutCmd(GPIOA, GPIO_Pin_7, ENABLE);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_7, ENABLE);
    GPIO_ClearBits(GPIOA, GPIO_Pin_7);

    delay = DELAY_VALUE;
    while (delay) {
        delay--;
    }
    GPIO_SetBits(GPIOA, GPIO_Pin_7);
}

void wdt_init()
{
    //инициализируем сторожевой таймер
    RCU_WDTClkConfig(RCU_SysPeriphClk_RefClk, 7, ENABLE);
    RCU_WDTClkCmd(ENABLE);
    RCU_WDTRstCmd(ENABLE);

    WDT_SetLoad(0x10000);
    WDT_RstCmd(ENABLE);
    WDT_Cmd(ENABLE);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    gpio_init();
    wdt_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); //1ms tick

    while (1) {
    };
    return 0;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
