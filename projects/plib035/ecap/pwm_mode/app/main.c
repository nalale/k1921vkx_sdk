/*==============================================================================
 * Пример, использующий функции драйвера CAP, чтобы организовать
 * 2 ШИМ сигнала, генерирующиеся в противофазе.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void ecap_init()
{
    // инициализация блоков захвата
    RCU_APBClkCmd(RCU_APBClk_ECAP1, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_ECAP1, ENABLE);
    RCU_APBClkCmd(RCU_APBClk_ECAP2, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_ECAP2, ENABLE);

    // выбираем PWM режим
    ECAP_Init_TypeDef ECAP_InitStruct;
    ECAP_StructInit(&ECAP_InitStruct);
    ECAP_InitStruct.Mode = ECAP_Mode_PWM;
    ECAP_InitStruct.SyncEn = ENABLE;
    ECAP_Init(ECAP1, &ECAP_InitStruct);
    ECAP_Init(ECAP2, &ECAP_InitStruct);

    // настраиваем PWM режим
    // 2 меандра в противофазе
    ECAP_PWM_Init_TypeDef ECAP_PWM_InitStruct;
    ECAP_PWM_StructInit(&ECAP_PWM_InitStruct);
    ECAP_PWM_InitStruct.Period = 0x3000;
    ECAP_PWM_InitStruct.Compare = 0x1800;
    ECAP_PWM_InitStruct.Polarity = ECAP_PWM_Polarity_Pos;
    ECAP_PWM_Init(ECAP1, &ECAP_PWM_InitStruct);
    ECAP_PWM_InitStruct.Polarity = ECAP_PWM_Polarity_Neg;
    ECAP_PWM_Init(ECAP2, &ECAP_PWM_InitStruct);

    // включаем счетчики - ШИМ начинает генерировать
    ECAP_TimerCmd(ECAP1, ENABLE);
    ECAP_TimerCmd(ECAP2, ENABLE);

    // синхронизируем счетчики для точной противофазы
    // в оба счетчика запишутся нули
    ECAP_SwSync(ECAP1);

    // инициализация выводов блоков ECAP1, ECAP2
    // так как вывод соответсвует выводу JTAG - он уже проинициализирован и включен по умолчанию,
    // нужно только включить ремап функции
    ECAP_RemapCmd(ECAP1, ENABLE);
    ECAP_RemapCmd(ECAP2, ENABLE);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    ecap_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    printf("PWM generating on A5, A6\n");

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
