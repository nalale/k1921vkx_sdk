/*==============================================================================
 * Пример выполнения операций с основной флеш: чтение, запись, стирание.
 * В примере читаются и выводятся в терминал последовательно 2 32-разрядных слова.
 * Затем происходит запись новых данных. Обновленные данные считываются,
 * а затем происходит стирание страницы с модифицированными адресами.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    uint32_t read_arr[2] = { 0x11111111, 0x22222222 };
    uint32_t write_arr[2] = { 0xDEADF00D, 0x79977997 };
    uint32_t addr = 0x00008000;

    periph_init();

    // читаем значения до всех операций
    MFLASH_ReadData(addr, read_arr, MFLASH_Region_Main);
    printf("Read flash before operation:\n");
    for (uint32_t i = 0; i < 2; i++) {
        printf("%d - 0x%08x - 0x%08x\n",
               (int)i,
               (unsigned int)(addr + 0x4 * i),
               (unsigned int)read_arr[i]);
    }

    // пишем значения
    printf("Performing write...\n");
    MFLASH_WriteData(addr, write_arr, MFLASH_Region_Main);

    // читаем и выводим новые значения
    printf("Write operation complete!\n");
    printf("Current values:\n");
    MFLASH_ReadData(addr, read_arr, MFLASH_Region_Main);
    for (uint32_t i = 0; i < 2; i++) {
        printf("%d - 0x%08x - 0x%08x\n",
               (int)i,
               (unsigned int)(addr + 0x4 * i),
               (unsigned int)read_arr[i]);
    }

    // стираем страницу, куда писали
    printf("Performing page erase...\n");
    MFLASH_ErasePage(addr, MFLASH_Region_Main);

    // читаем и выводим новые значения
    printf("Erase operation complete!\n");
    printf("Current values:\n");
    MFLASH_ReadData(addr, read_arr, MFLASH_Region_Main);
    for (uint32_t i = 0; i < 2; i++) {
        printf("%d - 0x%08x - 0x%08x\n",
               (int)i,
               (unsigned int)(addr + 0x4 * i),
               (unsigned int)read_arr[i]);
    }

    printf("The end!\n");
    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
