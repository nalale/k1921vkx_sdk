/*==============================================================================
 * Тест режима разборка-сборка (периферия) DMA.
 * Пересылка 32-битных значений из массива в массив по цепочке по 0 каналу.
 * Создаются 3 альтернативные конфигурации данных для осуществления пересылок
 * по цепочке:  * массив 0 -> массив 1 -> массив 2 -> массив 3.
 * Запускается процесс пересылки, затем проверяется соответсвие данных
 * массива 0 данным массива 3.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define TRANSFERS_TOTAL 32

#define __NOP10() \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP();      \
    __NOP()

#define __NOP100() \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10();     \
    __NOP10()

//-- Variables -----------------------------------------------------------------
DMA_CtrlData_TypeDef DMA_CTRLDATA __ALIGNED(512);
uint32_t array0[TRANSFERS_TOTAL];
uint32_t array1[TRANSFERS_TOTAL];
uint32_t array2[TRANSFERS_TOTAL];
uint32_t array3[TRANSFERS_TOTAL];
volatile uint32_t irq_status = 0;
DMA_Channel_TypeDef Task[3]; /* 3 набора альтернативных управляющих структур */

//-- Peripheral init functions -------------------------------------------------
void dma_init()
{
    /* Базовый указатель */
    DMA_BasePtrConfig((uint32_t)(&DMA_CTRLDATA));

    /* Инициализация канала */
    DMA_ChannelInit_TypeDef DMA_ChannelInitStruct;
    DMA_ChannelStructInit(&DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.SrcDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.DstDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.DstDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_4;
    DMA_ChannelInitStruct.TransfersTotal = 12;
    DMA_ChannelInitStruct.Mode = DMA_Mode_PrmPeriphScatGath;

    DMA_ChannelInitStruct.SrcDataEndPtr = &(Task[2].RESERVED);
    DMA_ChannelInitStruct.DstDataEndPtr = &(DMA_CTRLDATA.ALT_DATA.CH[DMA_CH_TMR0].RESERVED);
    DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[DMA_CH_TMR0], &DMA_ChannelInitStruct);

    /* Инициализация контроллера */
    DMA_Init_TypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.Channel = DMA_Channel_TMR0;
    DMA_InitStruct.ChannelEnable = ENABLE;
    DMA_Init(&DMA_InitStruct);
    DMA_MasterEnableCmd(ENABLE);

    /* инициализация альтернативных структур */
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_32;
    DMA_ChannelInitStruct.TransfersTotal = TRANSFERS_TOTAL;
    DMA_ChannelInitStruct.Mode = DMA_Mode_AltPeriphScatGath;
    DMA_ChannelInitStruct.SrcDataEndPtr = &array0[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[0], &DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataEndPtr = &array1[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[1], &DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.Mode = DMA_Mode_Basic;
    DMA_ChannelInitStruct.SrcDataEndPtr = &array2[TRANSFERS_TOTAL - 1];
    DMA_ChannelInitStruct.DstDataEndPtr = &array3[TRANSFERS_TOTAL - 1];
    DMA_ChannelInit(&Task[2], &DMA_ChannelInitStruct);

    /* NVIC init */
    NVIC_EnableIRQ(DMA_CH9_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    dma_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    /* инициализация массивов */
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        array0[i] = 0x10000001;
        array1[i] = 0x11011011;
        array2[i] = 0x12022021;
        array3[i] = 0x13033031;
    }

    /* Режим рассчитан на периодичные запросы от периферии, поэтому
    *  будем использовать таймер (хотя можно использовать и программную генерацию запросов).
    */
    DMA_ChannelMuxConfig(DMA_ChannelMux_9, DMA_ChannelMux_9_TMR0);
    RCU_APBClkCmd(RCU_APBClk_TMR0, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_TMR0, ENABLE);
    TMR_FreqConfig(TMR0, SystemCoreClock, 10000);
    TMR_DMAReqCmd(TMR0, ENABLE);
    TMR_Cmd(TMR0, ENABLE);

    while (irq_status != DMA_Channel_TMR0) {
    };

    /* проверяем результаты */
    uint32_t check_error = 0;
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        printf("array3[%d]=0x%08x; array2[%d]=0x%08x; array1[%d]=0x%08x; array0[%d]=0x%08x\n",
               (int)i,
               (unsigned int)array3[i],
               (int)i,
               (unsigned int)array2[i],
               (int)i,
               (unsigned int)array1[i],
               (int)i,
               (unsigned int)array0[i]);
        if (array0[i] != array3[i])
            check_error++;
    }

    printf("All transfers done with %d errors!\n", (int)check_error);

    while (1) {
    };

    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void DMA_CH9_IRQHandler()
{
    TMR_Cmd(TMR0, DISABLE);
    irq_status |= DMA_Channel_TMR0;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
