/*==============================================================================
 * Тест базового режима DMA.
 * Пересылка 32-битных значений из массива в массив по нулевому каналу
 * по программному запросу.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define TRANSFERS_TOTAL 8

//-- Variables -----------------------------------------------------------------
DMA_CtrlData_TypeDef DMA_CTRLDATA __ALIGNED(512);
uint32_t array_src[TRANSFERS_TOTAL];
uint32_t array_dst[TRANSFERS_TOTAL];

//-- Peripheral init functions -------------------------------------------------
void dma_init()
{
    /* Базовый указатель */
    DMA_BasePtrConfig((uint32_t)(&DMA_CTRLDATA));

    /* Инициализация канала */
    DMA_ChannelInit_TypeDef DMA_ChannelInitStruct;
    DMA_ChannelStructInit(&DMA_ChannelInitStruct);
    DMA_ChannelInitStruct.SrcDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.SrcDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.DstDataSize = DMA_DataSize_32;
    DMA_ChannelInitStruct.DstDataInc = DMA_DataInc_32;
    DMA_ChannelInitStruct.ArbitrationRate = DMA_ArbitrationRate_8;
    DMA_ChannelInitStruct.TransfersTotal = TRANSFERS_TOTAL;
    DMA_ChannelInitStruct.Mode = DMA_Mode_Basic;
    DMA_ChannelInitStruct.SrcDataEndPtr = &(array_src[TRANSFERS_TOTAL - 1]);
    DMA_ChannelInitStruct.DstDataEndPtr = &(array_dst[TRANSFERS_TOTAL - 1]);
    DMA_ChannelInit(&DMA_CTRLDATA.PRM_DATA.CH[0], &DMA_ChannelInitStruct);

    /* Инициализация контроллера */
    DMA_Init_TypeDef DMA_InitStruct;
    DMA_StructInit(&DMA_InitStruct);
    DMA_InitStruct.Channel = DMA_Channel_0;
    DMA_InitStruct.ChannelEnable = ENABLE;
    DMA_Init(&DMA_InitStruct);
    DMA_MasterEnableCmd(ENABLE);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    dma_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    /* инициализация массивов */
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        array_src[i] = 0xDEADBEEF;
        array_dst[i] = 0xCAFEBABE;
    }

    /* начинаем пересылку */
    DMA_SwRequestCmd(DMA_Channel_0);

    /* ждем пока dma выполняет запросы */
    while (DMA_ChannelEnableStatus(DMA_Channel_0)) {
    };

    /* проверяем результаты */
    uint32_t check_error = 0;
    for (uint32_t i = 0; i < TRANSFERS_TOTAL; i++) {
        printf("array_dst[%d]=0x%08x; array_src[%d]=0x%08x\n",
               (int)i,
               (unsigned int)array_dst[i],
               (int)i,
               (unsigned int)array_src[i]);
        if (array_src[i] != array_dst[i])
            check_error++;
    }

    printf("All transfers done with %d errors!\n", (int)check_error);

    while (1) {
    };

    return 0;
}

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
