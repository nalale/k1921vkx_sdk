/*==============================================================================
 * Пример, использующий функции драйвера PWM, чтобы организовать простую
 * генерацию ШИМ сигналов. PWM1 канал B - генерируется ШИМ сигнал 50кГц с
 * заполнением 2/3.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "plib035.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void pwm_init()
{
    PWM_TB_Init_TypeDef PWM_TB_InitStruct;
    PWM_CMP_Init_TypeDef PWM_CMP_InitStruct;
    PWM_AQ_Init_TypeDef PWM_AQ_InitStruct;
    GPIO_Init_TypeDef GPIO_InitStruct;

    //включаем тактирование и выводим из сброса
    RCU_APBClkCmd(RCU_APBClk_PWM1, ENABLE);
    RCU_APBRstCmd(RCU_APBRst_PWM1, ENABLE);

    //настраиваем таймер
    PWM_TB_StructInit(&PWM_TB_InitStruct);
    PWM_TB_InitStruct.Mode = PWM_TB_Mode_Up;
    // тактовую частоту 100МГц делим на 20 чтобы получить TBCLK=5МГц
    PWM_TB_InitStruct.ClkDiv = PWM_TB_ClkDiv_2;
    PWM_TB_InitStruct.ClkDivExtra = PWM_TB_ClkDivExtra_10;
    // период задаем таким, чтобы получить частоту ШИМ 50кГц
    PWM_TB_InitStruct.Period = 99;
    PWM_TB_Init(PWM1, &PWM_TB_InitStruct);

    //настраиваем компаратор
    PWM_CMP_StructInit(&PWM_CMP_InitStruct);
    PWM_CMP_InitStruct.CmpB = 66; // 2/3 от периода
    PWM_CMP_Init(PWM1, &PWM_CMP_InitStruct);

    //настраиваем поведение выходов
    PWM_AQ_StructInit(&PWM_AQ_InitStruct);
    PWM_AQ_InitStruct.ActionB_CTREqCMPBUp = PWM_AQ_Action_ToZero; // в 0 по сравнению
    PWM_AQ_InitStruct.ActionB_CTREqPeriod = PWM_AQ_Action_ToOne;  // в 1 по периоду
    PWM_AQ_Init(PWM1, &PWM_AQ_InitStruct);

    //настраиваем пины
    RCU_AHBClkCmd(RCU_AHBClk_GPIOA, ENABLE);
    RCU_AHBRstCmd(RCU_AHBRst_GPIOA, ENABLE);
    GPIO_StructInit(&GPIO_InitStruct);
    GPIO_InitStruct.AltFunc = ENABLE;
    GPIO_InitStruct.Pin = GPIO_Pin_11; //PWM1_B
    GPIO_Init(GPIOA, &GPIO_InitStruct);
    GPIO_DigitalCmd(GPIOA, GPIO_Pin_11, ENABLE);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    pwm_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    printf("Start PWM generation ...\n");

    PWM_TB_PrescCmd(PWM_TB_Presc_1, ENABLE);

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------

//-- Assert --------------------------------------------------------------------
#if defined USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
    printf("Assert failed: file %s on line %d\n", file, (int)line);
    while (1) {
    };
}
#endif /* USE_FULL_ASSERT */
