/*==============================================================================
 * Программное определение области, из которой исполняется текущий код
 * (косвенное определение состояния BOOT0, BOOT1 на момент сброса):
 *  - MFLASH
 *  - BFLASH
 *  - RAM0
 *  - EXTMEM
 *
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define GLOBMEM(ADDR) *(volatile uint32_t*)(ADDR)

//-- Peripheral init functions -------------------------------------------------
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
void remap_check()
{
    static const uint32_t BFARVALID_MASK = (0x80 << SCB_CFSR_BUSFAULTSR_Pos);

    SCB->CFSR |= BFARVALID_MASK;

    uint32_t mask = __get_FAULTMASK();
    __disable_fault_irq();
    SCB->CCR |= SCB_CCR_BFHFNMIGN_Msk;

    GLOBMEM(MEM_MFLASH_SIZE);
    if (!(SCB->CFSR & BFARVALID_MASK)) {
        printf("Remap: EXTMEM\n");
        BSP_LED_On(LED6_MSK);
        BSP_LED_On(LED7_MSK);
    } else {
        SCB->CFSR |= BFARVALID_MASK;
        GLOBMEM(MEM_BFLASH_SIZE);
        if (!(SCB->CFSR & BFARVALID_MASK)) {
            printf("Remap: MFLASH\n");
            BSP_LED_Off(LED6_MSK);
            BSP_LED_Off(LED7_MSK);
        } else {
            SCB->CFSR |= BFARVALID_MASK;
            GLOBMEM(MEM_RAM0_SIZE);
            if (!(SCB->CFSR & BFARVALID_MASK)) {
                printf("Remap: BFLASH\n");
                BSP_LED_On(LED6_MSK);
                BSP_LED_Off(LED7_MSK);
            } else {
                SCB->CFSR |= BFARVALID_MASK;
                printf("Remap: RAM0\n");
                BSP_LED_Off(LED6_MSK);
                BSP_LED_On(LED7_MSK);
            }
        }
    }

    SCB->CCR &= ~SCB_CCR_BFHFNMIGN_Msk;
    __set_FAULTMASK(mask);
    __enable_fault_irq();
}

int main()
{
    periph_init();
    remap_check();
    SysTick_Config(10000000);

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //Heartbit
    BSP_LED_Toggle(LED0_MSK);
}
