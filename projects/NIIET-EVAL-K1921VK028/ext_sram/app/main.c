/*==============================================================================
 * Пример чтения и записи во внешнею ОЗУ размером 128кБ
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------
#define DATA_WORDS 8

//-- Peripheral init functions -------------------------------------------------
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    BSP_SRAM_Init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    uint32_t data_wr[DATA_WORDS] = { 0x80818283,
                                     0x94959697,
                                     0xA8A9AAAB,
                                     0xBCBDBEBF,
                                     0xC0C1C2C3,
                                     0xD4D5D6D7,
                                     0xE8E9EAEB,
                                     0x12345678 };
    uint32_t data_rd[DATA_WORDS];
    int addr_offset = 0x4;

    periph_init();
    SysTick_Config(10000000);

    printf("Write to external SRAM:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        BSP_SRAM_Write(i * addr_offset, data_wr[i]);
        printf("%d: 0x%05x <- 0x%08x\n", i, i * addr_offset, (int)data_wr[i]);
    }
    printf("Read from external SRAM:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        data_rd[i] = BSP_SRAM_Read(i * addr_offset);
        printf("%d: 0x%05x -> 0x%08x\n", i, i * addr_offset, (int)data_rd[i]);
    }
    printf("Check results:\n");
    for (int i = 0; i < DATA_WORDS; i++) {
        if (data_rd[i] != data_wr[i])
            printf("%d: addr 0x%05x error, expected 0x%08x, got 0x%08x\n", i, i * addr_offset, (int)data_wr[i], (int)data_rd[i]);
        else
            printf("%d: addr 0x%05x ok\n", i, i * addr_offset);
    }

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //Heartbit
    BSP_LED_Toggle(LED0_MSK);
}
