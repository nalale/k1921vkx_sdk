/*==============================================================================
 * Получение данных с сигма-дельта модулятора AMC1303M052 каждую секунду
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void sdfm_init()
{
    // IO init
    // SDFM_DATA2 - A8 af1
    // SDFM_CLK2  - A9 af1
    RCU->HCLKCFG_bit.GPIOAEN = 1;
    RCU->HRSTCFG_bit.GPIOAEN = 1;
    GPIOA->ALTFUNCNUM1_bit.PIN8 = 1;
    GPIOA->ALTFUNCNUM1_bit.PIN9 = 1;
    GPIOA->ALTFUNCSET = GPIO_ALTFUNCSET_PIN8_Msk | GPIO_ALTFUNCSET_PIN9_Msk;
    GPIOA->DENSET = GPIO_DENSET_PIN8_Msk | GPIO_DENSET_PIN9_Msk;

    //config SDFM
    RCU->HCLKCFG_bit.SDFMEN = 1;
    RCU->HRSTCFG_bit.SDFMEN = 1;
    SDFM->SD[2].CTLPARM_bit.MOD = SDFM_SD_CTLPARM_MOD_Mode0SDR;
    SDFM->SD[2].DFPARM_bit.SST = SDFM_SD_DFPARM_SST_Sinc3;
    SDFM->SD[2].DFPARM_bit.DOSR = 31; // range [-32768;32768]
    SDFM->SD[2].FFCTL_bit.IL = 6;
    SDFM->SD[2].FFCTL_bit.DRISEL = SDFM_SD_FFCTL_DRISEL_FIFOData;
    SDFM->SD[2].FFCTL_bit.IEN = 1;
    SDFM->SD[2].FFCTL_bit.EN = 1;
    SDFM->MFILEN_bit.MFE = 1; // master enable

    SDFM->CTL_bit.MIE = 1; //master interrupt enable
    NVIC_EnableIRQ(SDFM_IRQn);
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    sdfm_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
volatile uint32_t new_result = 0;
volatile int32_t result;
int main()
{
    periph_init();
    SysTick_Config(SystemCoreClock / 1000 - 1); // 1ms tick

    while (1) {
        if (new_result) {
            new_result = 0;
            // IC Voltage range +-50 mV;
            // Current sensor 0.05 Ohm => Current range +-1000 mA
            // +-32768 is range for DOSR=31 and Sinc3 used
            printf("Current = %f mA (%0d)\n", (float)result * (1000.0f / 32768), (int16_t)result);
        }
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
volatile uint32_t tick_counter = 0;
void SysTick_Handler()
{
    if (tick_counter < 1000)
        tick_counter++;
    else {
        SDFM->SD[2].DFPARM_bit.FEN = 1; //enable filter
        tick_counter = 0;
        BSP_LED_Toggle(LED0_MSK);
    }
}

void SDFM_IRQHandler(void)
{
    if (SDFM->IFLG_bit.FDR2 != 0) {
        (void)SDFM->SD[2].DATA;
        (void)SDFM->SD[2].DATA;
        result = (int32_t)SDFM->SD[2].DATA;
        result += (int32_t)SDFM->SD[2].DATA;
        result += (int32_t)SDFM->SD[2].DATA;
        result += (int32_t)SDFM->SD[2].DATA;
        result >>= 2;                   // 4 samples average
        SDFM->SD[2].DFPARM_bit.FEN = 0; // disable filter
        SDFM->IFLGCLR = SDFM_IFLGCLR_FDR2_Msk;
        new_result = 1;
    }
}
