/*==============================================================================
 * Счетчик Джонсона на 64 JK-триггерах. Нужна внешняя обратная связь M[8]->A[0].
 * Внешнее тактирование подается на D[1].
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void lau_init()
{

    RCU->PCLKCFG1_bit.LAUEN = 1;
    RCU->PRSTCFG1_bit.LAUEN = 1;

    LAU->LM[0].CLKCTL = 0x00013FFA;
    LAU->LM[0].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[0].OUTMUX = 0x76543210;
    LAU->LM[0].OECTL = 0x0000FFFC;
    LAU->LM[0].LUTMUX0 = 0x0000071E;
    LAU->LM[0].LUTGATE0 = 0x00000210;
    LAU->LM[0].LUTPLF0 = 0x0000000F;
    LAU->LM[0].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[0].LUTGATE1 = 0x00001001;
    LAU->LM[0].LUTPLF1 = 0x0000000F;
    LAU->LM[0].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[0].LUTGATE2 = 0x00001001;
    LAU->LM[0].LUTPLF2 = 0x0000000F;
    LAU->LM[0].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[0].LUTGATE3 = 0x00001001;
    LAU->LM[0].LUTPLF3 = 0x0000000F;
    LAU->LM[0].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[0].LUTGATE4 = 0x00001001;
    LAU->LM[0].LUTPLF4 = 0x0000000F;
    LAU->LM[0].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[0].LUTGATE5 = 0x00001001;
    LAU->LM[0].LUTPLF5 = 0x0000000F;
    LAU->LM[0].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[0].LUTGATE6 = 0x00001001;
    LAU->LM[0].LUTPLF6 = 0x0000000F;
    LAU->LM[0].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[0].LUTGATE7 = 0x00001001;
    LAU->LM[0].LUTPLF7 = 0x0000000F;
    LAU->LM[1].CLKCTL = 0x00013FFA;
    LAU->LM[1].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[1].OUTMUX = 0x76543210;
    LAU->LM[1].OECTL = 0x0000FFFF;
    LAU->LM[1].LUTMUX0_bit.SEL0 = 0x00000017;
    LAU->LM[1].LUTGATE0 = 0x00001001;
    LAU->LM[1].LUTPLF0 = 0x0000000F;
    LAU->LM[1].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[1].LUTGATE1 = 0x00001001;
    LAU->LM[1].LUTPLF1 = 0x0000000F;
    LAU->LM[1].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[1].LUTGATE2 = 0x00001001;
    LAU->LM[1].LUTPLF2 = 0x0000000F;
    LAU->LM[1].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[1].LUTGATE3 = 0x00001001;
    LAU->LM[1].LUTPLF3 = 0x0000000F;
    LAU->LM[1].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[1].LUTGATE4 = 0x00001001;
    LAU->LM[1].LUTPLF4 = 0x0000000F;
    LAU->LM[1].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[1].LUTGATE5 = 0x00001001;
    LAU->LM[1].LUTPLF5 = 0x0000000F;
    LAU->LM[1].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[1].LUTGATE6 = 0x00001001;
    LAU->LM[1].LUTPLF6 = 0x0000000F;
    LAU->LM[1].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[1].LUTGATE7 = 0x00001001;
    LAU->LM[1].LUTPLF7 = 0x0000000F;
    LAU->LM[2].CLKCTL = 0x00013FFA;
    LAU->LM[2].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[2].OUTMUX = 0x76543210;
    LAU->LM[2].OECTL = 0x0000FFFF;
    LAU->LM[2].LUTMUX0_bit.SEL0 = 0x00000018;
    LAU->LM[2].LUTGATE0 = 0x00001001;
    LAU->LM[2].LUTPLF0 = 0x0000000F;
    LAU->LM[2].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[2].LUTGATE1 = 0x00001001;
    LAU->LM[2].LUTPLF1 = 0x0000000F;
    LAU->LM[2].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[2].LUTGATE2 = 0x00001001;
    LAU->LM[2].LUTPLF2 = 0x0000000F;
    LAU->LM[2].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[2].LUTGATE3 = 0x00001001;
    LAU->LM[2].LUTPLF3 = 0x0000000F;
    LAU->LM[2].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[2].LUTGATE4 = 0x00001001;
    LAU->LM[2].LUTPLF4 = 0x0000000F;
    LAU->LM[2].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[2].LUTGATE5 = 0x00001001;
    LAU->LM[2].LUTPLF5 = 0x0000000F;
    LAU->LM[2].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[2].LUTGATE6 = 0x00001001;
    LAU->LM[2].LUTPLF6 = 0x0000000F;
    LAU->LM[2].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[2].LUTGATE7 = 0x00001001;
    LAU->LM[2].LUTPLF7 = 0x0000000F;
    LAU->LM[3].CLKCTL = 0x00013FFA;
    LAU->LM[3].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[3].OUTMUX = 0x76543210;
    LAU->LM[3].OECTL = 0x0000FFFF;
    LAU->LM[3].LUTMUX0_bit.SEL0 = 0x00000019;
    LAU->LM[3].LUTGATE0 = 0x00001001;
    LAU->LM[3].LUTPLF0 = 0x0000000F;
    LAU->LM[3].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[3].LUTGATE1 = 0x00001001;
    LAU->LM[3].LUTPLF1 = 0x0000000F;
    LAU->LM[3].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[3].LUTGATE2 = 0x00001001;
    LAU->LM[3].LUTPLF2 = 0x0000000F;
    LAU->LM[3].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[3].LUTGATE3 = 0x00001001;
    LAU->LM[3].LUTPLF3 = 0x0000000F;
    LAU->LM[3].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[3].LUTGATE4 = 0x00001001;
    LAU->LM[3].LUTPLF4 = 0x0000000F;
    LAU->LM[3].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[3].LUTGATE5 = 0x00001001;
    LAU->LM[3].LUTPLF5 = 0x0000000F;
    LAU->LM[3].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[3].LUTGATE6 = 0x00001001;
    LAU->LM[3].LUTPLF6 = 0x0000000F;
    LAU->LM[3].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[3].LUTGATE7 = 0x00001001;
    LAU->LM[3].LUTPLF7 = 0x0000000F;
    LAU->LM[4].CLKCTL = 0x00013FFA;
    LAU->LM[4].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[4].OUTMUX = 0x76543210;
    LAU->LM[4].OECTL = 0x0000FFFF;
    LAU->LM[4].LUTMUX0_bit.SEL0 = 0x0000001A;
    LAU->LM[4].LUTGATE0 = 0x00001001;
    LAU->LM[4].LUTPLF0 = 0x0000000F;
    LAU->LM[4].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[4].LUTGATE1 = 0x00001001;
    LAU->LM[4].LUTPLF1 = 0x0000000F;
    LAU->LM[4].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[4].LUTGATE2 = 0x00001001;
    LAU->LM[4].LUTPLF2 = 0x0000000F;
    LAU->LM[4].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[4].LUTGATE3 = 0x00001001;
    LAU->LM[4].LUTPLF3 = 0x0000000F;
    LAU->LM[4].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[4].LUTGATE4 = 0x00001001;
    LAU->LM[4].LUTPLF4 = 0x0000000F;
    LAU->LM[4].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[4].LUTGATE5 = 0x00001001;
    LAU->LM[4].LUTPLF5 = 0x0000000F;
    LAU->LM[4].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[4].LUTGATE6 = 0x00001001;
    LAU->LM[4].LUTPLF6 = 0x0000000F;
    LAU->LM[4].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[4].LUTGATE7 = 0x00001001;
    LAU->LM[4].LUTPLF7 = 0x0000000F;
    LAU->LM[5].CLKCTL = 0x00013FFA;
    LAU->LM[5].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[5].OUTMUX = 0x76543210;
    LAU->LM[5].OECTL = 0x0000FFFF;
    LAU->LM[5].LUTMUX0_bit.SEL0 = 0x0000001B;
    LAU->LM[5].LUTGATE0 = 0x00001001;
    LAU->LM[5].LUTPLF0 = 0x0000000F;
    LAU->LM[5].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[5].LUTGATE1 = 0x00001001;
    LAU->LM[5].LUTPLF1 = 0x0000000F;
    LAU->LM[5].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[5].LUTGATE2 = 0x00001001;
    LAU->LM[5].LUTPLF2 = 0x0000000F;
    LAU->LM[5].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[5].LUTGATE3 = 0x00001001;
    LAU->LM[5].LUTPLF3 = 0x0000000F;
    LAU->LM[5].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[5].LUTGATE4 = 0x00001001;
    LAU->LM[5].LUTPLF4 = 0x0000000F;
    LAU->LM[5].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[5].LUTGATE5 = 0x00001001;
    LAU->LM[5].LUTPLF5 = 0x0000000F;
    LAU->LM[5].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[5].LUTGATE6 = 0x00001001;
    LAU->LM[5].LUTPLF6 = 0x0000000F;
    LAU->LM[5].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[5].LUTGATE7 = 0x00001001;
    LAU->LM[5].LUTPLF7 = 0x0000000F;
    LAU->LM[6].CLKCTL = 0x00013FFA;
    LAU->LM[6].BUSSEL_bit.LMNUM = 0x00000007;
    LAU->LM[6].OUTMUX = 0x76543210;
    LAU->LM[6].OECTL = 0x0000FFFF;
    LAU->LM[6].LUTMUX0_bit.SEL0 = 0x0000001C;
    LAU->LM[6].LUTGATE0 = 0x00001001;
    LAU->LM[6].LUTPLF0 = 0x0000000F;
    LAU->LM[6].LUTMUX1_bit.SEL0 = 0x0000000F;
    LAU->LM[6].LUTGATE1 = 0x00001001;
    LAU->LM[6].LUTPLF1 = 0x0000000F;
    LAU->LM[6].LUTMUX2_bit.SEL0 = 0x00000010;
    LAU->LM[6].LUTGATE2 = 0x00001001;
    LAU->LM[6].LUTPLF2 = 0x0000000F;
    LAU->LM[6].LUTMUX3_bit.SEL0 = 0x00000011;
    LAU->LM[6].LUTGATE3 = 0x00001001;
    LAU->LM[6].LUTPLF3 = 0x0000000F;
    LAU->LM[6].LUTMUX4_bit.SEL0 = 0x00000012;
    LAU->LM[6].LUTGATE4 = 0x00001001;
    LAU->LM[6].LUTPLF4 = 0x0000000F;
    LAU->LM[6].LUTMUX5_bit.SEL0 = 0x00000013;
    LAU->LM[6].LUTGATE5 = 0x00001001;
    LAU->LM[6].LUTPLF5 = 0x0000000F;
    LAU->LM[6].LUTMUX6_bit.SEL0 = 0x00000014;
    LAU->LM[6].LUTGATE6 = 0x00001001;
    LAU->LM[6].LUTPLF6 = 0x0000000F;
    LAU->LM[6].LUTMUX7_bit.SEL0 = 0x00000015;
    LAU->LM[6].LUTGATE7 = 0x00001001;
    LAU->LM[6].LUTPLF7 = 0x0000000F;
    LAU->LM[7].CLKCTL = 0x00013FFA;
    LAU->LM[7].BUSSEL_bit.LMNUM = 0x00000001;
    LAU->LM[7].OUTMUX = 0x76543210;
    LAU->LM[7].OECTL = 0x0000FFFF;
    LAU->LM[7].LUTMUX0_bit.SEL0 = 0x00000010;
    LAU->LM[7].LUTGATE0 = 0x00001001;
    LAU->LM[7].LUTPLF0 = 0x0000000F;
    LAU->LM[7].LUTMUX1_bit.SEL0 = 0x00000011;
    LAU->LM[7].LUTGATE1 = 0x00001001;
    LAU->LM[7].LUTPLF1 = 0x0000000F;
    LAU->LM[7].LUTMUX2_bit.SEL0 = 0x00000012;
    LAU->LM[7].LUTGATE2 = 0x00001001;
    LAU->LM[7].LUTPLF2 = 0x0000000F;
    LAU->LM[7].LUTMUX3_bit.SEL0 = 0x00000013;
    LAU->LM[7].LUTGATE3 = 0x00001001;
    LAU->LM[7].LUTPLF3 = 0x0000000F;
    LAU->LM[7].LUTMUX4_bit.SEL0 = 0x00000014;
    LAU->LM[7].LUTGATE4 = 0x00001001;
    LAU->LM[7].LUTPLF4 = 0x0000000F;
    LAU->LM[7].LUTMUX5_bit.SEL0 = 0x00000015;
    LAU->LM[7].LUTGATE5 = 0x00001001;
    LAU->LM[7].LUTPLF5 = 0x0000000F;
    LAU->LM[7].LUTMUX6_bit.SEL0 = 0x00000016;
    LAU->LM[7].LUTGATE6 = 0x00001001;
    LAU->LM[7].LUTPLF6 = 0x0000000F;
    LAU->LM[7].LUTMUX7_bit.SEL0 = 0x0000001D;
    LAU->LM[7].LUTGATE7 = 0x00001001;
    LAU->LM[7].LUTPLF7 = 0x0000000F;

    RCU->HCLKCFG_bit.GPIOAEN = 1;
    RCU->HRSTCFG_bit.GPIOAEN = 1;
    GPIOA->ALTFUNCNUM0 = 0x44444444;
    GPIOA->ALTFUNCNUM1 = 0x44444444;
    GPIOA->ALTFUNCSET = 0xFFFFFFFF;
    GPIOA->DENSET = 0xFFFFFFFF;

    RCU->HCLKCFG_bit.GPIODEN = 1;
    RCU->HRSTCFG_bit.GPIODEN = 1;
    GPIOD->ALTFUNCNUM0_bit.PIN1 = 4;
    GPIOD->ALTFUNCSET = GPIO_ALTFUNCSET_PIN1_Msk;
    GPIOD->DENSET = GPIO_ALTFUNCSET_PIN1_Msk;

    RCU->HCLKCFG_bit.GPIOMEN = 1;
    RCU->HRSTCFG_bit.GPIOMEN = 1;
    GPIOM->ALTFUNCNUM1_bit.PIN8 = 3;
    GPIOM->ALTFUNCSET = GPIO_ALTFUNCSET_PIN8_Msk;
    GPIOM->DENSET = GPIO_ALTFUNCSET_PIN8_Msk;
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    lau_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    while (1) {
    };
    return 0;
}
