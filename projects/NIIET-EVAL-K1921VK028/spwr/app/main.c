/*==============================================================================
 * Использование двух модулей SpaceWire в режиме echo - возвращают всё
 * что им передано
 *------------------------------------------------------------------------------
 * НИИЭТ, Дмитрий Сериков <lonie@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"
#include "stdio.h"

//-- Defines -------------------------------------------------------------------
// TX Enable + TX Interrupt + Packet Send
#define TXD_DEFVAL ((SPWR_TXD_CTRL_EN_Msk) | \
                    (SPWR_TXD_CTRL_WR_Msk) | \
                    (SPWR_TXD_CTRL_IE_Msk))
// Wrap + Enable + IntEnable
#define RXD_DEFVAL ((SPWR_RXD_CTRL_EN_Msk) | \
                    (SPWR_RXD_CTRL_WR_Msk) | \
                    (SPWR_RXD_CTRL_IE_Msk))

#define DMACSR_RX ((SPWR_DMACSR_RE_Msk) | \
                   (SPWR_DMACSR_RI_Msk) | \
                   (SPWR_DMACSR_PR_Msk) | \
                   (SPWR_DMACSR_RD_Msk))
// RX Enable + RX Interrupt + Packet Receive + RX Desc active
#define DMACSR_TX ((SPWR_DMACSR_TE_Msk) | \
                   (SPWR_DMACSR_TI_Msk) | \
                   (SPWR_DMACSR_PS_Msk))

#define BUF_SIZE 64

//-- Variables -----------------------------------------------------------------
volatile uint8_t RXBuf0[BUF_SIZE] __ALIGNED(4);
volatile uint8_t TXBuf0[BUF_SIZE] __ALIGNED(4);

volatile uint8_t RXBuf1[BUF_SIZE] __ALIGNED(4);
volatile uint8_t TXBuf1[BUF_SIZE] __ALIGNED(4);

SPWR_RXD_TypeDef RXD[2] __ALIGNED(16);
SPWR_TXD_TypeDef TXD[2] __ALIGNED(16);

//-- Peripheral init functions -------------------------------------------------
void spwr_init()
{
    RCU->HCLKCFG_bit.GPIOLEN = 1;
    RCU->HRSTCFG_bit.GPIOLEN = 1;
    GPIOL->ALTFUNCNUM1_bit.PIN12 = 4; //SPWR0 DIN
    GPIOL->ALTFUNCNUM1_bit.PIN13 = 4; //SPWR0 SIN
    GPIOL->ALTFUNCNUM1_bit.PIN14 = 4; //SPWR0 DOUT
    GPIOL->ALTFUNCNUM1_bit.PIN15 = 4; //SPWR0 DOUT
    GPIOL->ALTFUNCSET = 0xF000;
    GPIOL->DENSET = 0xF000;

    RCU->HCLKCFG_bit.GPIOMEN = 1;
    RCU->HRSTCFG_bit.GPIOMEN = 1;
    GPIOM->ALTFUNCNUM1_bit.PIN12 = 4; //SPWR1 DIN
    GPIOM->ALTFUNCNUM1_bit.PIN13 = 4; //SPWR1 SIN
    GPIOM->ALTFUNCNUM1_bit.PIN14 = 4; //SPWR1 DOUT
    GPIOM->ALTFUNCNUM1_bit.PIN15 = 4; //SPWR1 DOUT
    GPIOM->ALTFUNCSET = 0xF000;
    GPIOM->DENSET = 0xF000;

    RCU->HCLKCFG_bit.SPWR0EN = 1;
    RCU->HCLKCFG_bit.SPWR1EN = 1;
    RCU->HRSTCFG_bit.SPWR0EN = 1;
    RCU->HRSTCFG_bit.SPWR1EN = 1;

    RCU->SPWRCFG[0].SPWRCFG_bit.CLKEN = 1;
    RCU->SPWRCFG[1].SPWRCFG_bit.CLKEN = 1;

    SPWR0->DEFADDR_bit.DM = 0xFF; //any addr
    SPWR1->DEFADDR_bit.DM = 0xFF;

    SPWR0->CTRL_bit.LS = 1; //link start
    SPWR1->CTRL_bit.LS = 1;
    SPWR0->CTRL_bit.AS = 1; //link autostart
    SPWR1->CTRL_bit.AS = 1;

    SPWR0->CTRL_bit.IE = 1; //interrupt enable
    SPWR1->CTRL_bit.IE = 1;

    SPWR0->CLKDIV_bit.CKDRUN = 0; //clock divisor in run state (0+1) = 1
    SPWR1->CLKDIV_bit.CKDRUN = 0;
    SPWR0->CLKDIV_bit.CKDSTRT = 18; //clock divisor in startup (18+1) = 19
    SPWR1->CLKDIV_bit.CKDSTRT = 18;

    SPWR0->DMACSR = (SPWR_DMACSR_PS_Msk |
                     SPWR_DMACSR_PR_Msk |
                     SPWR_DMACSR_TA_Msk |
                     SPWR_DMACSR_RA_Msk); //packet sent, pack recv, TX ahb err, RX ahb err
    SPWR1->DMACSR = (SPWR_DMACSR_PS_Msk |
                     SPWR_DMACSR_PR_Msk |
                     SPWR_DMACSR_TA_Msk |
                     SPWR_DMACSR_RA_Msk);
    SPWR0->DMARXLEN = BUF_SIZE; //rx len
    SPWR1->DMARXLEN = BUF_SIZE;

    NVIC_EnableIRQ(SPWR0_IRQn);
    NVIC_EnableIRQ(SPWR1_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    spwr_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(15000000);

    SPWR0->DMATDTADDR = (uint32_t)&TXD[0];
    SPWR0->DMARDTADDR = (uint32_t)&RXD[0];
    SPWR1->DMATDTADDR = (uint32_t)&TXD[1];
    SPWR1->DMARDTADDR = (uint32_t)&RXD[1];

    RXD[0].PKTADDR = (uint32_t)&RXBuf0;
    RXD[0].WORDS[0] = RXD_DEFVAL;
    SPWR0->DMACSR = DMACSR_RX;

    RXD[1].PKTADDR = (uint32_t)&RXBuf0;
    RXD[1].WORDS[0] = RXD_DEFVAL;
    SPWR1->DMACSR = DMACSR_RX;

    printf("SPWR0, SPWR1 are ready ...\n");

    while (1) {
        if (SPWR0->STAT_bit.LS == SPWR_STAT_LS_Run)
            BSP_LED_On(LED6_MSK);
        else
            BSP_LED_Off(LED6_MSK);

        if (SPWR1->STAT_bit.LS == SPWR_STAT_LS_Run)
            BSP_LED_On(LED7_MSK);
        else
            BSP_LED_Off(LED7_MSK);
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle(LED0_MSK);
}

void SPWR0_IRQHandler()
{
    if (SPWR0->DMACSR_bit.TP) {
        SPWR0->DMACSR = SPWR_DMACSR_TP_Msk;

        RXD[0].PKTADDR = (uint32_t)&RXBuf0;
        RXD[0].WORDS[0] = RXD_DEFVAL;

        SPWR0->DMARDTADDR = (uint32_t)&RXD[0];
        SPWR0->DMACSR = DMACSR_RX;

        BSP_LED_Toggle(LED2_MSK);
    }
    if (SPWR0->DMACSR_bit.RP) {
        SPWR0->DMACSR = SPWR_DMACSR_RP_Msk;

        TXD[0].DATAADDR = (uint32_t)&RXBuf0;
        TXD[0].DATALEN = RXD[0].WORDS[0] & 0xff;
        TXD[0].HEADERADDR = 0;
        TXD[0].WORDS[0] = TXD_DEFVAL;

        SPWR0->DMATDTADDR = (uint32_t)&TXD[0];
        SPWR0->DMACSR = DMACSR_TX;

        BSP_LED_Toggle(LED3_MSK);
    }
}

void SPWR1_IRQHandler()
{
    if (SPWR1->DMACSR_bit.TP) // first receive, copy buffers
    {
        SPWR1->DMACSR = SPWR_DMACSR_TP_Msk;

        RXD[1].PKTADDR = (uint32_t)&RXBuf1;
        RXD[1].WORDS[0] = RXD_DEFVAL;

        SPWR1->DMARDTADDR = (uint32_t)&RXD[1];
        SPWR1->DMACSR = DMACSR_RX;

        BSP_LED_Toggle(LED4_MSK);
    }
    if (SPWR1->DMACSR_bit.RP) // answer transmitted
    {
        SPWR1->DMACSR = SPWR_DMACSR_RP_Msk;

        TXD[1].DATAADDR = (uint32_t)&RXBuf1;
        TXD[1].DATALEN = RXD[1].WORDS[0] & 0xff;
        TXD[1].HEADERADDR = 0;
        TXD[1].WORDS[0] = TXD_DEFVAL;

        SPWR1->DMATDTADDR = (uint32_t)&TXD[1];
        SPWR1->DMACSR = DMACSR_TX;

        BSP_LED_Toggle(LED5_MSK);
    }
}
