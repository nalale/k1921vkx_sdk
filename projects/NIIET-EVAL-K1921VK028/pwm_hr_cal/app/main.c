/*==============================================================================
 * Вычисление реальной задержки в элементах HR модуля ШИМ
 *
 *------------------------------------------------------------------------------
 * НИИЭТ, Роман Степаненко <hgost@niiet.ru>
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void pwm_init()
{
    RCU->HCLKCFG_bit.GPIOLEN = 1;
    RCU->HRSTCFG_bit.GPIOLEN = 1;
    RCU->PCLKCFG1 = RCU_PCLKCFG1_PWM1EN_Msk;
    RCU->PRSTCFG1 = RCU_PRSTCFG1_PWM1EN_Msk;

    GPIOL->DENSET = GPIO_DENSET_PIN2_Msk | GPIO_DENSET_PIN3_Msk;
    GPIOL->ALTFUNCSET = GPIO_DENSET_PIN2_Msk | GPIO_DENSET_PIN3_Msk;
    GPIOL->ALTFUNCNUM0 = (GPIOL->ALTFUNCNUM1 & ~(0xFF00)) | 0x2200;

    PWM1->TBPRD = 0x800;
    PWM1->CMPA_bit.CMPA = 0x400;
    PWM1->AQCTLA_bit.CAU = PWM_AQCTLA_CAU_Set;
    PWM1->AQCTLA_bit.CAD = PWM_AQCTLA_CAD_Clear;
    PWM1->TBCTL_bit.CTRMODE = PWM_TBCTL_CTRMODE_UpDown;
    PWM1->HRCTL_bit.EDGMODEA = PWM_HRCTL_EDGMODEA_BothEdge;
    PWM1->ETSEL = PWM_ETSEL_INTSEL_CTREqPRD << PWM_ETSEL_INTSEL_Pos |
                  PWM_ETSEL_INTEN_Msk;
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    pwm_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();
    SysTick_Config(10000000);

    uint32_t hr_cntr = 0;
    uint32_t ctr0 = 0;
    uint32_t ctr1 = 0;

    SIU->PWMSYNC_bit.PRESCRST = 0x1 << 1; //start PWM1
    PWM1->HRCTL_bit.DELAYCALA = 0;
    while ((hr_cntr < 255) && (ctr1 == 0)) {
        uint32_t temp = PWM1->HRCTL_bit.DELAYCALA;

        if ((ctr0 == 0) && (temp)) {
            ctr0 = PWM1->CMPA_bit.CMPAHR;
            hr_cntr += hr_cntr/2;
            PWM1->HRCTL_bit.DELAYCALA = 0;
        }
        else if ((ctr1 == 0) && (temp == 3))
            ctr1 = PWM1->CMPA_bit.CMPAHR;
        else hr_cntr++;

        PWM1->CMPA_bit.CMPAHR = hr_cntr;

        //wait 2 PWM periods
        for (int i=0;i<2;i++) {

            while (!PWM1->ETFLG_bit.INT) {
            };
            PWM1->INTCLR = PWM_INTCLR_INT_Msk;
            PWM1->ETCLR = PWM_ETCLR_INT_Msk;
        }
    }

    uint32_t pwm_t_ps = 1E12 / APB0BusClock;
    uint32_t res_e = pwm_t_ps / (ctr1 - ctr0);
    uint32_t res_m = pwm_t_ps - ctr0 * res_e;

    printf("ctr0 = %d, ctr1 = %d\n", (int)ctr0, (int)ctr1);
    printf("One element delay: %d ps\n", (int)res_e);
    printf("Mux delay: %d ps\n", (int)res_m);

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle(LED0_MSK);
}
