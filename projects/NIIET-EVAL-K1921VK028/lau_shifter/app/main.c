/*==============================================================================
 * Бегущий светодиод с помощью сдвигового регистра построенного на LAU.
 * Используется внешний тактовый сигнал, который необходимо подать на D[1].
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include "bsp.h"
#include "retarget_conf.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void lau_init()
{
    RCU->PCLKCFG1_bit.LAUEN = 1;
    RCU->PRSTCFG1_bit.LAUEN = 1;

    LAU->LM[0].CLKCTL = 0x00013FFA;
    LAU->LM[0].OUTMUX = 0x76543210;
    LAU->LM[0].OECTL = 0x0000FFFF;
    LAU->LM[0].LUTMUX0_bit.SEL0 = 0x0000000E;
    LAU->LM[0].LUTGATE0_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF0 = 0x00000009;
    LAU->LM[0].LUTMUX1_bit.SEL0 = 0x00000007;
    LAU->LM[0].LUTGATE1_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF1 = 0x00000009;
    LAU->LM[0].LUTMUX2_bit.SEL0 = 0x00000008;
    LAU->LM[0].LUTGATE2_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF2 = 0x00000009;
    LAU->LM[0].LUTMUX3_bit.SEL0 = 0x00000009;
    LAU->LM[0].LUTGATE3_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF3 = 0x00000009;
    LAU->LM[0].LUTMUX4_bit.SEL0 = 0x0000000A;
    LAU->LM[0].LUTGATE4_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF4 = 0x00000009;
    LAU->LM[0].LUTMUX5_bit.SEL0 = 0x0000000B;
    LAU->LM[0].LUTGATE5_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF5 = 0x00000009;
    LAU->LM[0].LUTMUX6_bit.SEL0 = 0x0000000C;
    LAU->LM[0].LUTGATE6_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF6 = 0x00000009;
    LAU->LM[0].LUTMUX7_bit.SEL0 = 0x0000000D;
    LAU->LM[0].LUTGATE7_bit.GP1 = 0x00000001;
    LAU->LM[0].LUTPLF7 = 0x00000009;

    LAU->LM[0].LUTGATE0 = 0x00000188;
    LAU->LM[0].LUTGATE1 = 0x00000188;
    LAU->LM[0].LUTGATE2 = 0x00000188;
    LAU->LM[0].LUTGATE3 = 0x00000188;
    LAU->LM[0].LUTGATE4 = 0x00000188;
    LAU->LM[0].LUTGATE5 = 0x00000188;
    LAU->LM[0].LUTGATE6 = 0x00000188;
    LAU->LM[0].LUTGATE7 = 0x00000188;

    LAU->LM[0].CLKCTL_bit.CLKEN = 0x00000000;
    LAU->LM[0].LUTGATE0 = 0x00000100;
    LAU->LM[0].LUTGATE1 = 0x00000000;
    LAU->LM[0].LUTGATE2 = 0x00000100;
    LAU->LM[0].LUTGATE3 = 0x00000100;
    LAU->LM[0].LUTGATE4 = 0x00000100;
    LAU->LM[0].LUTGATE5 = 0x00000100;
    LAU->LM[0].LUTGATE6 = 0x00000100;
    LAU->LM[0].LUTGATE7 = 0x00000100;

    LAU->LM[0].LUTGATE0 = 0x88000100;
    LAU->LM[0].CLKCTL_bit.CLKEN = 0x00000001;

    LAU->LM[0].CLKCTL_bit.CLKEN = 0x00000000;
    LAU->LM[0].LUTGATE0 = 0x00000100;

    LAU->LM[0].LUTGATE1_bit.GP1 = 0x00000001;
    LAU->LM[0].CLKCTL_bit.CLKEN = 0x00000001;

    RCU->HCLKCFG_bit.GPIOAEN = 1;
    RCU->HRSTCFG_bit.GPIOAEN = 1;
    GPIOA->ALTFUNCNUM0 = 0x44444444;
    GPIOA->ALTFUNCSET = 0x0000FFFF;
    GPIOA->DENSET = 0x0000FFFF;

    RCU->HCLKCFG_bit.GPIODEN = 1;
    RCU->HRSTCFG_bit.GPIODEN = 1;
    GPIOD->ALTFUNCNUM0_bit.PIN1 = 4;
    GPIOD->ALTFUNCSET = GPIO_ALTFUNCSET_PIN1_Msk;
    GPIOD->DENSET = GPIO_ALTFUNCSET_PIN1_Msk;
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    lau_init();
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------
int main()
{
    periph_init();

    while (1) {
    };
    return 0;
}
