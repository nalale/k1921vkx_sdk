/*==============================================================================
 * Пример чтения и записи во внешнею ОЗУ.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2020 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK01T.h"
#include "bsp.h"
#include "retarget_conf.h"
#include "system_K1921VK01T.h"

//-- Defines -------------------------------------------------------------------
// 16 bit mask -> 64k (CE0) + 64k (CE1)
// RAM on CE1
#define EXTRAM(ADDR) *(volatile uint32_t*)(0x40010000 + (uint32_t)(ADDR))

//-- Peripheral init functions -------------------------------------------------
void extmem_init()
{
    // A[15:8] = MEM_ADDR[7:0]
    NT_COMMON_REG->GPIODENA |= 0xFF00;
    NT_GPIOA->ALTFUNCSET = 0xFF << 8;

    // B[15] = MEM_DATA[0], B[14:4] = MEM_ADDR[18:8]
    NT_COMMON_REG->GPIODENB |= 0xFFF0;
    NT_GPIOB->ALTFUNCSET = 0xFFF0;

    // C[15:6] = MEM_DATA[10:1]
    NT_COMMON_REG->GPIODENC |= 0xFFC0;
    NT_GPIOC->ALTFUNCSET = 0xFFC0;

    // D[15:12] = MEM_DATA[14:11]
    NT_COMMON_REG->GPIODEND |= 0xF000;
    NT_GPIOD->ALTFUNCSET = 0xF000;

    // E[14] = MEM_Oen0, E[15] = MEM_Oen1, E[12] = MEM_DATA[15], E[13] = MEM_Wen
    NT_COMMON_REG->GPIODENE |= 0xF000;
    NT_GPIOE->ALTFUNCSET = 0xF000;

    // F[9] = MEM_Ubn, F[8] = MEM_Lbn, F[7] = MEM_Cen1, F[6] = MEM_Cen0}
    NT_COMMON_REG->GPIODENF |= 0x03C0;
    NT_GPIOF->ALTFUNCSET = 0x03C0;

    NT_COMMON_REG->EXT_MEM_CFG = (1 << COMMON_REG_EXT_MEM_CFG_BW_Pos) | // 16 bit mode
                                 (7 << COMMON_REG_EXT_MEM_CFG_RWWS_Pos) |
                                 (7 << COMMON_REG_EXT_MEM_CFG_ReadWS_Pos) |
                                 (7 << COMMON_REG_EXT_MEM_CFG_WriteWS_Pos) |
                                 (0x20 << COMMON_REG_EXT_MEM_CFG_CE_MASK_Pos); // 16 bit masked
}

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    extmem_init();
}

//-- Main ----------------------------------------------------------------------

int main()
{
    volatile uint32_t temp;

    periph_init();
    printf("K1921VK01T> All periph inited\n");
    printf("K1921VK01T> CPU frequency is %.3f MHz\n", SystemCoreClock / 1E6);

    printf("Write to external RAM:\n");
    for (uint32_t i = 0; i < 8; i++) {
        temp = 0x1ACDBEEF + i * 0x2A;
        EXTRAM(i * 0x4 * 0x2) = temp;
        printf("    %d: 0x%x - 0x%x\n", i, i * 0x4 * 0x2, temp);
    }

    printf("Read from external RAM:\n");
    for (uint32_t i = 0; i < 8; i++) {
        temp = EXTRAM(i * 0x4 * 0x2);
        printf("    %d: 0x%x - 0x%x\n", i, i * 0x4 * 0x2, temp);
    }

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
