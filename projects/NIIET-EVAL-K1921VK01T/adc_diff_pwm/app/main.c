/*==============================================================================
 * Используются PWM6, секвенсор 0 и ADC0.
 * Канал 0 (канал A ADC0) работает в дифф режиме и измеряет напряжение
 * относительно канала 1 (канал B ADC0).
 * Преобразование запускается по событию сравнения A PWM6.
 *
 * ШИМ настроен на максимально низкую частоту - запуск измерения происходит
 * примерно раз в секунду.
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK01T.h"
#include "bsp.h"
#include "retarget_conf.h"
#include "system_K1921VK01T.h"

//-- Defines -------------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------
void pwm_init()
{
    uint32_t tmp;

    //включаем тактирование и выводим из сброса
    NT_COMMON_REG->APB_CLK_bit.PWMEN6 = 1;
    NT_COMMON_REG->PER_RST1_bit.PWMRST6 = 1;

    //настраиваем таймер
    NT_PWM6->AQCTLA_bit.PRD = 2; // CTR==PERIOD => chA to "1"
    //TBCLK ~ 55804Hz
    NT_PWM6->TBCTL_bit.CLKDIV = 7; // 1/128
    NT_PWM6->TBCTL_bit.HSPCLKDIV = 7; // 1/14
    //TBPRD ~ 1sec
    NT_PWM6->TBCTL_bit.CTRMODE = 0; // count up
    NT_PWM6->TBPRD = 55803;

    //настраиваем компаратор
    NT_PWM6->AQCTLA_bit.CAU = 1; // UP && CTR==CMPA => chA to "0"
    NT_PWM6->CMPA_bit.CMPA = 55804/2 - 1; // 50% от периода

    //настраиваем триггер событий
    NT_PWM6->ETSEL_bit.SOCASEL = 4; // UP && CTR==CMPA
    NT_PWM6->ETSEL_bit.SOCAEN = 1;

    //настраиваем пин PWM0 A - G14 AF3
    tmp = (1 << 14);
    NT_GPIOG->ALTFUNCSET |= tmp;
    NT_COMMON_REG->GPIOPCTLG_bit.PIN14 = 2;
    NT_COMMON_REG->GPIODENG |= tmp;
}

void adc_init()
{
    NT_COMMON_REG->APB_CLK_bit.ADCEN = 1;
    // хак, чтобы избежать зависших секвенсоров от выставленных запросов по 0 каналу
    // цифровыми компараторами. Хотя в данном случае это необязательно, т.к. 0-ой канал используется.
    for (uint32_t i = 0; i < 24; i++) {
        NT_ADC->DCCTL_bit[i].CHNL = 0x1F;
    }

    // включаем тактирование 0-модуля ацп (0 и 1 каналы)
    // делим системную частоту на 8, и получаем тактирование АЦП 12,5МГц.
    NT_COMMON_REG->ADC_CTRL0_bit.DIV_ADC0 = 3;
    NT_COMMON_REG->ADC_CTRL0_bit.DIVEN_ADC0 = 1;
    NT_COMMON_REG->ADC_CTRL0_bit.CLKEN_ADC0 = 1;

    // настройка модуля АЦП0
    NT_ADC->SAC_bit.AVG0 = 4;          // avg 16
    NT_ADC->PP_bit[0].OM = ((0 << 6) | // ch B
                            (1 << 4) | // diff on ch A (A-B)
                            (0 << 3) | // 12 bit
                            (3 << 0)); // modeactive
    NT_ADC->PP_bit[0].ENA = 1;         // включение модуля АЦП

    // настройка секвенсора 0
    NT_ADC->EMUX_bit.EM0 = 0xA;    //старт по SOCA PWM 6, 7, 8
    NT_ADC->SEQ[0].MUX = 1 << 0; // ch 0

    // включаем прерывание секвенсора после каждого измерения
    NT_ADC->SEQ[0].CTL_bit.ICNT = 0;
    NT_ADC->IM_bit.MASK0 = 1;
    NVIC_EnableIRQ(ADC_SEQ0_IRQn);
}
void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    pwm_init();
    adc_init();
    printf("All periph inited, K1921VK01T frequency is %d Hz\n", (int)SystemCoreClock);
}

//-- Main ----------------------------------------------------------------------
volatile uint32_t adc_irq_flag = 0;

int main()
{
    uint32_t res;

    periph_init();
    SysTick_Config(10000000);

    printf("Start diff measure CH0-CH1 from PWM6 CMPA event\n");
    NT_ADC->ACTSS_bit.ASEN0 = 1;      //enable seq0
    NT_COMMON_REG->PWM_SYNC = 1 << 6; //enable pwm6

    while (1) {
        if (adc_irq_flag) {
            adc_irq_flag = 0;
            BSP_LED_Toggle(LED1_MSK);
            res = NT_ADC->SEQ[0].FIFO_bit.DATA;
            printf("%0.4fV | 0x%03x | %04d\n", 1.5f*((int)res-2048)/2048, res, res);
        }
    }
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    BSP_LED_Toggle(LED0_MSK);
}

void ADC_SEQ0_IRQHandler()
{
    NT_ADC->ISC = 1 << 0; //seq0
    adc_irq_flag = 1;
}
