/*==============================================================================
 * Пример использования SPI для работы с микросхемой EEPROM 93c46
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2019 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "K1921VK01T.h"
#include "at93c46.h"
#include "bsp.h"
#include "retarget_conf.h"
#include "system_K1921VK01T.h"

//-- Defines -------------------------------------------------------------------

//-- Variables -----------------------------------------------------------------

//-- Peripheral init functions -------------------------------------------------

void periph_init()
{
    SystemCoreClockUpdate();
    retarget_init();
    BSP_LED_Init();
    AT93C46_Init();
    SysTick_Config(10000000);
    printf("\nAll peripherals inited, SYSCLK = %3d MHz\n", (int)(SystemCoreClock / 1E6));
}

//-- Main ----------------------------------------------------------------------

int main()
{
    periph_init();

    printf("AT93C46 Write Enable and Erase All\n");
    AT93C46_WriteEnable();
    AT93C46_EraseAll();

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write:\n");
    for (uint16_t i = 0; i < 8; i++) {
        uint16_t wdata = 0x8000 + i * 3 + (i * 4 << 8);
        AT93C46_Write(i, wdata);
        printf("    Addr[%d] = 0x%04x\n", (int)i, wdata);
    }

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Erase:\n");
    for (uint16_t i = 0; i < 8; i++) {
        if ((i + 1) % 2 != 0) {
            AT93C46_Erase(i);
            printf("    Addr[%d]\n", (int)i);
        }
    }

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write All 0xeeee\n");
    AT93C46_WriteAll(0xEEEE);

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Erase All and Write Disable\n");
    AT93C46_EraseAll();
    AT93C46_WriteDisable();

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    printf("AT93C46 Write All 0xeeee\n");
    AT93C46_WriteAll(0xEEEE);

    printf("AT93C46 Read:\n");
    for (uint16_t i = 0; i < 8; i++)
        printf("    Addr[%d] = 0x%04x\n", (int)i, AT93C46_Read(i));

    while (1) {
    };
    return 0;
}

//-- IRQ handlers --------------------------------------------------------------
void SysTick_Handler()
{
    //BSP_LED_Toggle();
}
