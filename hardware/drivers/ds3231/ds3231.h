/*==============================================================================
 * Описание регистрового пространства микросхемы RTC DS3231
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

#ifndef __DS3231_H
#define __DS3231_H

// Start of section using anonymous unions
#if defined (__CC_ARM)
  #pragma push
  #pragma anon_unions
#elif defined (__ICCARM__)
  #pragma language=extended
#elif defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
  #pragma clang diagnostic push
  #pragma clang diagnostic ignored "-Wc11-extensions"
  #pragma clang diagnostic ignored "-Wreserved-id-macro"
#elif defined (__GNUC__)
  // anonymous unions are enabled by default
#elif defined (__TMS470__)
  // anonymous unions are enabled by default
#elif defined (__TASKING__)
  #pragma warning 586
#elif defined (__CSMC__)
  // anonymous unions are enabled by default
#elif defined (__CMCPPARM__)
  // anonymous unions are enabled by default 
#else
  #warning Not supported compiler type
#endif

#ifdef __cplusplus
extern "C" {
#endif

//-- Includes ------------------------------------------------------------------
#include <stdio.h>
#include <stdint.h>

//-- Defines -------------------------------------------------------------------
#define DS3231_I2C_ADDR 0x68
#define DS3231_REG_ADDR(GLOBADDR) ((uint8_t)((uintptr_t)&GLOBADDR) & DS3231_REGS_ALIGN_MSK)
#define DS3231_REG_PTR(GLOBADDR) ((uint8_t*)&GLOBADDR)
#define DS3231_REGS_ALIGN_VAL 32
#define DS3231_REGS_ALIGN_MSK 0x1F

//-- Registers descriptions  ---------------------------------------------------
//-- SEC reg: Seconds
typedef struct {
    uint8_t VAL : 7; // Seconds 00-59
    uint8_t : 1;
} _DS3231_SEC_bits;

//-- MIN reg: Minutes
typedef struct {
    uint8_t VAL : 7; // Minutes 00-59
} _DS3231_MIN_bits;

//-- HOUR12 reg: Hours in 12h mode
typedef struct {
    uint8_t VAL : 5;    // Hours 1-12
    uint8_t AMPM : 1;   // AM/PM mode
    uint8_t H12H24 : 1; // 12/24 hours mode
} _DS3231_HOUR12_bits;

//-- HOUR24 reg: Hours in 24h mode
typedef struct {
    uint8_t VAL : 6;    // Hours 00-23
    uint8_t H12H24 : 1; // 12/24 hours mode
} _DS3231_HOUR24_bits;

//-- DAY reg: Day of the week
typedef struct {
    uint8_t VAL : 3; // Day of the week 1-7
} _DS3231_DAY_bits;

//-- DATE reg: Date
typedef struct {
    uint8_t VAL : 6; // Date 1-31
} _DS3231_DATE_bits;

//-- MONTH reg: Month and century
typedef struct {
    uint8_t VAL : 5; // Month 01-12
    uint8_t : 2;
    uint8_t CENT : 1; // Century
} _DS3231_MONTH_bits;

//-- YEAR reg: Year
typedef struct {
    uint8_t VAL : 8; // Year 0-99
} _DS3231_YEAR_bits;

//-- A1SEC reg: Alarm 1 Seconds
typedef struct {
    uint8_t VAL : 7;  // Seconds 00-59
    uint8_t A1M1 : 1; // Alarm 1 mask 1
} _DS3231_A1SEC_bits;

//-- A1MIN reg: Alarm 1 Minutes
typedef struct {
    uint8_t VAL : 7;  // Minutes 00-59
    uint8_t A1M2 : 1; // Alarm 1 mask 2
} _DS3231_A1MIN_bits;

//-- A1HOUR12 reg: Alarm 1 Hours in 12h mode
typedef struct {
    uint8_t VAL : 5;    // Hours 1-12
    uint8_t AMPM : 1;   // AM/PM mode
    uint8_t H12H24 : 1; // 12/24 hours mode
    uint8_t A1M3 : 1;   // Alarm 1 mask 3
} _DS3231_A1HOUR12_bits;

//-- A1 HOUR24 reg: Alarm 1 Hours in 24h mode
typedef struct {
    uint8_t VAL : 6;    // Hours 00-23
    uint8_t H12H24 : 1; // 12/24 hours mode
    uint8_t A1M3 : 1;   // Alarm 1 mask 3
} _DS3231_A1HOUR24_bits;

//-- A1DAY reg: Alarm 1 Day of the week
typedef struct {
    uint8_t VAL : 3; // Day of the week 1-7
    uint8_t : 3;
    uint8_t DYDT : 1; // Day/Date select
    uint8_t A1M4 : 1; // Alarm 1 mask 4
} _DS3231_A1DAY_bits;

//-- A1DATE reg: Alarm 1 Date
typedef struct {
    uint8_t VAL : 6;  // Date 1-31
    uint8_t DYDT : 1; // Day/Date select
    uint8_t A1M4 : 1; // Alarm 1 mask 4
} _DS3231_A1DATE_bits;

//-- A2MIN reg: Alarm 2 Minutes
typedef struct {
    uint8_t VAL : 7;  // Minutes 00-59
    uint8_t A2M1 : 1; // Alarm 2 mask 1
} _DS3231_A2MIN_bits;

//-- A2HOUR12 reg: Alarm 2 Hours in 12h mode
typedef struct {
    uint8_t VAL : 5;    // Hours 1-12
    uint8_t AMPM : 1;   // AM/PM mode
    uint8_t H12H24 : 1; // 12/24 hours mode
    uint8_t A2M2 : 1;   // Alarm 2 mask 2
} _DS3231_A2HOUR12_bits;

//-- A2 HOUR24 reg: Alarm 2 Hours in 24h mode
typedef struct {
    uint8_t VAL : 6;    // Hours 00-23
    uint8_t H12H24 : 1; // 12/24 hours mode
    uint8_t A2M2 : 1;   // Alarm 2 mask 2
} _DS3231_A2HOUR24_bits;

//-- A2DAY reg: Alarm 2 Day of the week
typedef struct {
    uint8_t VAL : 3; // Day of the week 1-7
    uint8_t : 3;
    uint8_t DYDT : 1; // Day/Date select
    uint8_t A2M3 : 1; // Alarm 2 mask 3
} _DS3231_A2DAY_bits;

//-- A2DATE reg: Alarm 2 Date
typedef struct {
    uint8_t VAL : 6;  // Date 1-31
    uint8_t DYDT : 1; // Day/Date select
    uint8_t A2M3 : 1; // Alarm 2 mask 3
} _DS3231_A2DATE_bits;

//-- CTRL reg: Control
typedef struct {
    uint8_t A1IE : 1;  // Alarm 1 Interrupt Enable
    uint8_t A2IE : 1;  // Alarm 2 Interrupt Enable
    uint8_t INTCN : 1; // Interrupt Control
    uint8_t RS : 2;    // Rate Select
    uint8_t CONV : 1;  // Convert Temperature
    uint8_t BBSQW : 1; // Battery-Backed Square-Wave Enable
    uint8_t EOSC : 1;  // Enable Oscillator (active 0)
} _DS3231_CTRL_bits;

//-- STAT reg: Control/Status
typedef struct {
    uint8_t A1F : 1;     // Alarm 1 Flag
    uint8_t A2F : 1;     // Alarm 2 Flag
    uint8_t BSY : 1;     // Busy
    uint8_t EN32KHZ : 1; // Enable 32kHz Output
    uint8_t : 3;
    uint8_t OSF : 1; // Oscillator Stop Flag
} _DS3231_STAT_bits;

//-- OFFSET reg: Aging Offset
typedef struct {
    uint8_t DATA : 7; // Data
    uint8_t SIGN : 1; // Sign
} _DS3231_OFFSET_bits;

//-- TEMPH reg: MSB of Temp
typedef struct {
    uint8_t DATA : 7; // Data
    uint8_t SIGN : 1; // Sign
} _DS3231_TEMPH_bits;

//-- TEMPL reg: LSB of Temp
typedef struct {
    uint8_t : 6;
    uint8_t DATA : 2; // Data
} _DS3231_TEMPL_bits;

//-- Register map --------------------------------------------------------------
// This struct should be aligned!
typedef struct {
    union {
        volatile uint8_t SEC;
        volatile _DS3231_SEC_bits SEC_bit;
    };
    union {
        volatile uint8_t MIN;
        volatile _DS3231_MIN_bits MIN_bit;
    };
    union {
        volatile uint8_t HOUR;
        volatile _DS3231_HOUR12_bits HOUR12_bit;
        volatile _DS3231_HOUR24_bits HOUR24_bit;
    };
    union {
        volatile uint8_t DAY;
        volatile _DS3231_DAY_bits DAY_bit;
    };
    union {
        volatile uint8_t DATE;
        volatile _DS3231_DATE_bits DATE_bit;
    };
    union {
        volatile uint8_t MONTH;
        volatile _DS3231_MONTH_bits MONTH_bit;
    };
    union {
        volatile uint8_t YEAR;
        volatile _DS3231_YEAR_bits YEAR_bit;
    };
    union {
        volatile uint8_t A1SEC;
        volatile _DS3231_A1SEC_bits A1SEC_bit;
    };
    union {
        volatile uint8_t A1MIN;
        volatile _DS3231_A1MIN_bits A1MIN_bit;
    };
    union {
        volatile uint8_t A1HOUR;
        volatile _DS3231_A1HOUR12_bits A1HOUR12_bit;
        volatile _DS3231_A1HOUR24_bits A1HOUR24_bit;
    };
    union {
        volatile uint8_t A1DAYDATE;
        volatile _DS3231_A1DAY_bits A1DAY_bit;
        volatile _DS3231_A1DATE_bits A1DATE_bit;
    };
    union {
        volatile uint8_t A2MIN;
        volatile _DS3231_A2MIN_bits A2MIN_bit;
    };
    union {
        volatile uint8_t A2HOUR;
        volatile _DS3231_A2HOUR12_bits A2HOUR12_bit;
        volatile _DS3231_A2HOUR24_bits A2HOUR24_bit;
    };
    union {
        volatile uint8_t A2DAYDATE;
        volatile _DS3231_A2DAY_bits A2DAY_bit;
        volatile _DS3231_A2DATE_bits A2DATE_bit;
    };
    union {
        volatile uint8_t CTRL;
        volatile _DS3231_CTRL_bits CTRL_bit;
    };
    union {
        volatile uint8_t STAT;
        volatile _DS3231_STAT_bits STAT_bit;
    };
    union {
        volatile uint8_t OFFSET;
        volatile _DS3231_OFFSET_bits OFFSET_bit;
    };
    union {
        volatile uint8_t TEMPH;
        volatile _DS3231_TEMPH_bits TEMPH_bit;
    };
    union {
        volatile uint8_t TEMPL;
        volatile _DS3231_TEMPL_bits TEMPL_bit;
    };
} DS3231_TypeDef;

#ifdef __cplusplus
}
#endif

// End of section using anonymous unions
#if defined(__CC_ARM)
  #pragma pop
#elif defined(__ICCARM__)
  // leave anonymous unions enabled
#elif defined(__GNUC__)
  // anonymous unions are enabled by default
#elif defined(__TMS470__)
  // anonymous unions are enabled by default
#elif defined(__TASKING__)
  #pragma warning restore
#elif defined (__CMCPPARM__)
  // anonymous unions are enabled by default  
#else
  #warning Not supported compiler type
#endif

#endif // __DS3231_H
