/*==============================================================================
 * Управление периферией на плате КФДЛ.441461.19 (NIIET-EVAL-K1921VK028)
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

//-- Includes ------------------------------------------------------------------
#include "bsp.h"

//-- Private variables ---------------------------------------------------------

static volatile union {
    struct {
        uint32_t SB3 : 1;
        uint32_t SB4 : 1;
    } State_bits;
    BSP_Btn_State_TypeDef State;
} Buttons;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init()
{
    RCU->HCLKCFG_bit.LED_PORT_EN = 1;
    RCU->HRSTCFG_bit.LED_PORT_EN = 1;
    LED_PORT->DATAOUTSET = LED_PIN_MSK;
    LED_PORT->DENSET = LED_PIN_MSK;
    LED_PORT->OUTENSET = LED_PIN_MSK;
}

void BSP_LED_Toggle(uint32_t leds)
{
    LED_PORT->DATAOUTTGL = leds;
}

void BSP_LED_On(uint32_t leds)
{
    LED_PORT->DATAOUTCLR = leds;
}

void BSP_LED_Off(uint32_t leds)
{
    LED_PORT->DATAOUTSET = leds;
}

void BSP_Btn_Init()
{
    // Настройка выводов
    RCU->HCLKCFG_bit.BTN_PORT_EN = 1;
    RCU->HRSTCFG_bit.BTN_PORT_EN = 1;
    BTN_PORT->DENSET = BTN_SB3_PIN_MSK | BTN_SB4_PIN_MSK;
    BTN_PORT->INTTYPESET = (1 << BTN_SB3_PIN_POS) | (1 << BTN_SB4_PIN_POS); // фронт
    BTN_PORT->INTPOLSET = (1 << BTN_SB3_PIN_POS) | (1 << BTN_SB4_PIN_POS);  // положительный
    BTN_PORT->INTENSET = BTN_SB3_PIN_MSK | BTN_SB4_PIN_MSK;
    NVIC_EnableIRQ(BTN_IRQ_N);
}

BSP_Btn_State_TypeDef BSP_Btn_GetState(void)
{
    BSP_Btn_State_TypeDef state;

    state = Buttons.State;
    Buttons.State = BSP_Btn_State_None;

    return state;
}

void BSP_SRAM_Init()
{
    RCU->HCLKCFG |= RCU_HCLKCFG_GPIODEN_Msk |
                    RCU_HCLKCFG_GPIOEEN_Msk |
                    RCU_HCLKCFG_GPIOFEN_Msk |
                    RCU_HCLKCFG_GPIOGEN_Msk |
                    RCU_HCLKCFG_EXTMEMEN_Msk;
    RCU->HRSTCFG |= RCU_HRSTCFG_GPIODEN_Msk |
                    RCU_HRSTCFG_GPIOEEN_Msk |
                    RCU_HRSTCFG_GPIOFEN_Msk |
                    RCU_HCLKCFG_GPIOGEN_Msk |
                    RCU_HCLKCFG_EXTMEMEN_Msk;

    EXTMEM->WINCFG[2].WINCFG_bit.MODE = 1;
    EXTMEM->WINCFG[2].WINCFG_bit.RDCYC = 15;
    EXTMEM->WINCFG[2].WINCFG_bit.WRCYC = 15;
    EXTMEM->WINCFG[2].WINCFG_bit.TACYC = 15;

    //Control
    GPIOD->ALTFUNCNUM0_bit.PIN6 = 1;  //CE
    GPIOD->ALTFUNCNUM1_bit.PIN12 = 1; //LB
    GPIOD->ALTFUNCNUM1_bit.PIN13 = 1; //WE
    GPIOD->ALTFUNCNUM1_bit.PIN14 = 1; //UB
    GPIOD->ALTFUNCNUM1_bit.PIN15 = 1; //OE
    GPIOD->ALTFUNCSET = GPIO_ALTFUNCSET_PIN6_Msk |
                        GPIO_ALTFUNCSET_PIN12_Msk |
                        GPIO_ALTFUNCSET_PIN13_Msk |
                        GPIO_ALTFUNCSET_PIN14_Msk |
                        GPIO_ALTFUNCSET_PIN15_Msk;
    GPIOD->DENSET = GPIO_DENSET_PIN6_Msk |
                    GPIO_DENSET_PIN12_Msk |
                    GPIO_DENSET_PIN13_Msk |
                    GPIO_DENSET_PIN14_Msk |
                    GPIO_DENSET_PIN15_Msk;

    //Data
    GPIOE->ALTFUNCNUM0 = 0x11111111;
    GPIOE->ALTFUNCNUM1 = 0x11111111;
    GPIOE->ALTFUNCSET = 0xFFFFFFFF;
    GPIOE->DENSET = 0xFFFFFFFF;

    //Addr[15:1]
    GPIOF->ALTFUNCNUM0 = 0x11111111;
    GPIOF->ALTFUNCNUM1 = 0x11111111;
    GPIOF->ALTFUNCSET = 0xFFFFFFFF;
    GPIOF->DENSET = 0xFFFFFFFF;

    //Addr[16]
    GPIOG->ALTFUNCNUM0_bit.PIN0 = 0x1;
    GPIOG->ALTFUNCSET = GPIO_DENSET_PIN0_Msk;
    GPIOG->DENSET = GPIO_DENSET_PIN0_Msk;
}

void BSP_CAN_Init()
{
    // IO init only
    // D7 - CAN0_TX, D8 - CAN0_RX
    // D9 - CAN1_TX, D10 - CAN1_RX
    RCU->HCLKCFG_bit.GPIODEN = 1;
    RCU->HRSTCFG_bit.GPIODEN = 1;
    GPIOD->ALTFUNCSET = (0xF<<7);
    GPIOD->ALTFUNCNUM0_bit.PIN7 = 2;
    GPIOD->ALTFUNCNUM1_bit.PIN8 = 2;
    GPIOD->ALTFUNCNUM1_bit.PIN9 = 2;
    GPIOD->ALTFUNCNUM1_bit.PIN10 = 2;
    GPIOD->DENSET = (0xF<<7);
}

void BSP_ETH_Init(void)
{
    // MII_COL    - H15 (af1)
    // MII_CRS    - H14 (af1)
    // MII_TXER   - H13 (af1)
    // MII_TXEN   - H12 (af1)
    // MII_TXCLK  - H11 (af1)
    // MII_RXER   - H10 (af1)
    // MII_RXDV   - H9  (af1)
    // MII_RXCLK  - H8  (af1)
    // MII_RXD[3] - H7  (af1)
    // MII_RXD[2] - H6  (af1)
    // MII_RXD[1] - H5  (af1)
    // MII_RXD[0] - H4  (af1)
    // MII_TXD[3] - H3  (af1)
    // MII_TXD[2] - H2  (af1)
    // MII_TXD[1] - H1  (af1)
    // MII_TXD[0] - H0  (af1)
    RCU->HCLKCFG_bit.GPIOHEN = 1;
    RCU->HRSTCFG_bit.GPIOHEN = 1;
    GPIOH->ALTFUNCNUM0 = 0x11111111;
    GPIOH->ALTFUNCNUM1 = 0x11111111;
    GPIOH->ALTFUNCSET = 0xFFFFFFFF;
    GPIOH->DENSET = 0xFFFFFFFF;

    // MII_MDIO   - G14  (af1)
    // MII_MDC    - G15  (af1)
    RCU->HCLKCFG_bit.GPIOGEN = 1;
    RCU->HRSTCFG_bit.GPIOGEN = 1;
    GPIOG->ALTFUNCNUM1_bit.PIN14 = 1;
    GPIOG->ALTFUNCNUM1_bit.PIN15 = 1;
    GPIOG->ALTFUNCSET = GPIO_ALTFUNCSET_PIN14_Msk | GPIO_ALTFUNCSET_PIN15_Msk;
    GPIOG->DENSET = GPIO_DENSET_PIN14_Msk | GPIO_DENSET_PIN15_Msk;

    //TODO: add PHY interrupt init

    RCU->HCLKCFG_bit.ETHEN = 1;
    RCU->HRSTCFG_bit.ETHEN = 1;
}

//-- IRQ handlers --------------------------------------------------------------
void BTN_IRQ_HANDLER()
{
    if (BTN_PORT->INTSTATUS & BTN_SB3_PIN_MSK) {
        Buttons.State_bits.SB3 = 1;
        BTN_PORT->INTSTATUS = BTN_SB3_PIN_MSK;
    }

    if (BTN_PORT->INTSTATUS & BTN_SB4_PIN_MSK) {
        Buttons.State_bits.SB4 = 1;
        BTN_PORT->INTSTATUS = BTN_SB4_PIN_MSK;
    }
}
