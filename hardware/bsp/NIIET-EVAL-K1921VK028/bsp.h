/*==============================================================================
 * Определения для периферии платы КФДЛ.441461.19 (NIIET-EVAL-K1921VK028)
 *------------------------------------------------------------------------------
 * НИИЭТ, Богдан Колбов <kolbov@niiet.ru>
 *==============================================================================
 * ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ ПРЕДОСТАВЛЯЕТСЯ «КАК ЕСТЬ», БЕЗ КАКИХ-ЛИБО
 * ГАРАНТИЙ, ЯВНО ВЫРАЖЕННЫХ ИЛИ ПОДРАЗУМЕВАЕМЫХ, ВКЛЮЧАЯ ГАРАНТИИ ТОВАРНОЙ
 * ПРИГОДНОСТИ, СООТВЕТСТВИЯ ПО ЕГО КОНКРЕТНОМУ НАЗНАЧЕНИЮ И ОТСУТСТВИЯ
 * НАРУШЕНИЙ, НО НЕ ОГРАНИЧИВАЯСЬ ИМИ. ДАННОЕ ПРОГРАММНОЕ ОБЕСПЕЧЕНИЕ
 * ПРЕДНАЗНАЧЕНО ДЛЯ ОЗНАКОМИТЕЛЬНЫХ ЦЕЛЕЙ И НАПРАВЛЕНО ТОЛЬКО НА
 * ПРЕДОСТАВЛЕНИЕ ДОПОЛНИТЕЛЬНОЙ ИНФОРМАЦИИ О ПРОДУКТЕ, С ЦЕЛЬЮ СОХРАНИТЬ ВРЕМЯ
 * ПОТРЕБИТЕЛЮ. НИ В КАКОМ СЛУЧАЕ АВТОРЫ ИЛИ ПРАВООБЛАДАТЕЛИ НЕ НЕСУТ
 * ОТВЕТСТВЕННОСТИ ПО КАКИМ-ЛИБО ИСКАМ, ЗА ПРЯМОЙ ИЛИ КОСВЕННЫЙ УЩЕРБ, ИЛИ
 * ПО ИНЫМ ТРЕБОВАНИЯМ, ВОЗНИКШИМ ИЗ-ЗА ИСПОЛЬЗОВАНИЯ ПРОГРАММНОГО ОБЕСПЕЧЕНИЯ
 * ИЛИ ИНЫХ ДЕЙСТВИЙ С ПРОГРАММНЫМ ОБЕСПЕЧЕНИЕМ.
 *
 *                              2018 АО "НИИЭТ"
 *==============================================================================
 */

#ifndef BSP_H
#define BSP_H

#ifdef __cplusplus
extern "C" {
#endif

//-- Includes ------------------------------------------------------------------
#include "K1921VK028.h"
#include <stdio.h>

//-- Defines -------------------------------------------------------------------
//LEDs
#define LED_PORT GPIOA
#define LED_PORT_EN GPIOAEN
#define LED_PIN_MSK 0x00FF
#define LED0_POS 0
#define LED1_POS 1
#define LED2_POS 2
#define LED3_POS 3
#define LED4_POS 4
#define LED5_POS 5
#define LED6_POS 6
#define LED7_POS 7
#define LED0_MSK (1 << LED0_POS)
#define LED1_MSK (1 << LED1_POS)
#define LED2_MSK (1 << LED2_POS)
#define LED3_MSK (1 << LED3_POS)
#define LED4_MSK (1 << LED4_POS)
#define LED5_MSK (1 << LED5_POS)
#define LED6_MSK (1 << LED6_POS)
#define LED7_MSK (1 << LED7_POS)

//Buttons SB3, SB4
#define BTN_PORT GPIOG
#define BTN_PORT_EN GPIOGEN
#define BTN_IRQ_N GPIOG_IRQn
#define BTN_IRQ_HANDLER GPIOG_IRQHandler
#define BTN_SB3_PIN_POS 13
#define BTN_SB3_PIN_MSK (1 << BTN_SB3_PIN_POS)
#define BTN_SB4_PIN_POS 14
#define BTN_SB4_PIN_MSK (1 << BTN_SB4_PIN_POS)

//External memory
#define EXT_SRAM_BASE MEM_EXT2_BASE

//-- Types ---------------------------------------------------------------------
typedef enum {
    BSP_Btn_State_None,
    BSP_Btn_State_SB3,
    BSP_Btn_State_SB4,
    BSP_Btn_State_Both
} BSP_Btn_State_TypeDef;

//-- Functions -----------------------------------------------------------------
void BSP_LED_Init(void);
void BSP_LED_On(uint32_t leds);
void BSP_LED_Off(uint32_t leds);
void BSP_LED_Toggle(uint32_t leds);
void BSP_Btn_Init(void);
BSP_Btn_State_TypeDef BSP_Btn_GetState(void);
void BSP_SRAM_Init(void);

__STATIC_INLINE uint32_t BSP_SRAM_Read(uint32_t addr)
{
    return *(volatile uint32_t*)(EXT_SRAM_BASE + addr);
}

__STATIC_INLINE void BSP_SRAM_Write(uint32_t addr, uint32_t data)
{
    *(volatile uint32_t*)(EXT_SRAM_BASE + addr) = data;
}
void BSP_CAN_Init(void);
void BSP_ETH_Init(void);

#ifdef __cplusplus
}
#endif

#endif // BSP_H
